IBDE_HOME=$IBDE_PROJECT_PATH
scp $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar dm@srgssd-20:/home/dm/yanjie/exp/KafkaSpark-assembly-0.0.1.jar
scp -r $IBDE_HOME/conf dm@srgssd-20:$IBDE_PROJECT_PATH
scp -r $IBDE_HOME/exp dm@srgssd-20:$IBDE_PROJECT_PATH

./bin/spark-submit \
  --class workload.IBDE_Experiment \
  --master spark://10.190.172.43:7077 \
  --executor-memory 40G \
  --driver-memory 40g \
  /home/dm/yanjie/exp/KafkaSpark-assembly-0.0.1.jar