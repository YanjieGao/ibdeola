JAVA_DIR=/home/yanjie/jdk1.7.0_65
SPARK_HOME=/home/dm/yanjie/exp/spark-1.2.0-bin-hadoop2.4
cd $SPARK_HOME && ./bin/spark-submit \
  --class workload.IBDE_ParseGroupDistribute \
  --master spark://10.190.172.43:7077 \
  --executor-memory 40G \
  --driver-memory 40g \
  $IBDE_PROJECT_PATH/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar \
