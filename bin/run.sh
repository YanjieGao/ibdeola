filepath=$(cd "$(dirname "$0")"; pwd)
cd $filepath && cd .. && cd .. && cd util && chmod 777 config-env.sh && ./config-env.sh
PSSH_HOME=/home/dm/yanjie/exp/pssh
IBDE_HOME=$IBDE_PROJECT_PATH
IBDE_SPARK_HOME=/home/dm/yanjie/exp/spark-1.2.0-bin-hadoop2.4
JAVA_HOME=/home/dm/yanjie/exp/jdk1.7.0_65
SPARK_MASTER=spark://10.190.172.43:7077
cp $IBDE_HOME/conf/testola-config.properties $IBDE_HOME/conf/ola-config.properties

cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment  \
 --master $SPARK_MASTER   --executor-memory 40G --driver-memory 40g  \
 --conf spark.akka.frameSize=1024    \
 $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/testlog
echo "[exp]stop exp1-param1"