package olasystem.util

import java.io.{File, PrintWriter}

import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by yanjga on 9/14/2015.
 */
object DataSampleTools {
  def main (args: Array[String]) {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val data = sc.textFile("D:\\dataset\\lessColumn_vertical.txt")
    val tmp = data.take(10000)
    val writer = new PrintWriter(new File("D:\\dataset\\vertical_test.txt" ))
    tmp.foreach(item => {
      writer.println(item)
    })
    writer.close()
  }
}
