package olasystem.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;
import java.util.Set;

/**
 * Created by yanjga on 7/29/2015.
 */
public class RedisUtils {
    public static void main(String[] args) {
        // test Redis config
        JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost");
        /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            /// ... do stuff here ... for example
            jedis.set("foo", "testJedis");
            String foobar = jedis.get("foo");
            jedis.zadd("sose", 0, "car");
            jedis.zadd("sose", 0, "bike");
            Set<String> sose = jedis.zrange("sose", 0, -1);
            System.out.println(foobar);
        } finally {
            /// ... when closing your application:
            pool.destroy();
        }
    }
}
