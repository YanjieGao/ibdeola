package olasystem.util

/**
 * Created by yanjga on 9/1/2015.
 */
object TreeUtils {
  def fromRootIndex2LeafStartAndEnd(rootIndex: Int, leafStart: Int, leafEnd: Int): (Int, Int) = {
    var start = rootIndex
    var end = rootIndex
    while(!(start >= leafStart && start <= leafEnd)) {
      start *= 2
    }
    while(!(end >= leafStart && end <= leafEnd)) {
      end *= 2
      end += 1
    }
    (start, end)
  }
}
