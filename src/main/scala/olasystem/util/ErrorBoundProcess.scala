package olasystem.util

import org.apache.spark.util.collection.BitSet

/**
 * Created by yanjga on 7/29/2015.
 */
object ErrorBoundProcessUtils {
  def Data2ErrorBoundTree(aggData: Array[Double]): BitSet = {
    val hoeffdingJudge = new HoeffdingError
    hoeffdingJudge.config
    hoeffdingJudge.JudgeErrorBound(aggData)
  }
}

abstract class ErrorBoundStrategy{
  def config(): Unit = {

  }

  def JudgeErrorBound(data: Array[Double]): BitSet
}

class HoeffdingError extends  ErrorBoundStrategy {
  override def config: Unit = {

  }

  def JudgeErrorBound(aggData: Array[Double]): BitSet = {
    var bitMarkTree = new BitSet(2 * aggData.size - 1)
    bitMarkTree
  }
}