package olasystem

import olasystem.config.ConfigParam
import olasystem.datasource.{FileCustomReceiver, CustomReceiver}
import olasystem.execute.{UpdateFunction, MapFunction, ReduceFunction}
import olasystem.structure.{ErrorBoundTree, CompactBitIndexArray}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by yanjga on 7/10/2015.
 */
object MainEntryVersion1 {
  def main(args: Array[String]) {
    // (1) Load Data
    ConfigParam.localconfig()
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Seconds(1))
    val customReceiverStream = ssc.receiverStream(new CustomReceiver())
    //val customReceiverStream = ssc.receiverStream(new FileCustomReceiver())
    val words = customReceiverStream.map(_.split(" "))
    val kv = words.map(ws =>
      (ws(2), ws(4).toDouble)
    )
//    val kv = words.map(ws =>
//      (ws(0), ws(2).toDouble)
//    )
    //kv.print()
    // (2) Build Index

    // (3) Context Init

    // (4) Map Side Aggregation
    val mappedDStream = kv.mapPartitions(iter => {
      MapFunction.mapfunUseSkipList(iter)
    })
    // (5) Reduce Side Aggregation
    import StreamingContext.toPairDStreamFunctions
    val reducedDStream = mappedDStream.reduceByKey((a: CompactBitIndexArray, b: CompactBitIndexArray) =>{
      val combined = CompactBitIndexArray.combine2CompactBitIndexArray(a, b)
      combined
    })
    // (6) UpdateStateByKey
    val errorBoundTree = Array.fill(System.getProperty("ola.ola.TreeDepth").toInt)(0)
    val updatedDStream = reducedDStream.updateStateByKey((values: Seq[CompactBitIndexArray], state: Option[(CompactBitIndexArray, ErrorBoundTree)]) => {
      // (6.1) Aggregate the new value with the previous value
      var resultStateCompactBitIndexArray: CompactBitIndexArray = null
      var stateErrorBoundTree: ErrorBoundTree = null
      val old = state.get
      if(old == null) {
        val tree = new ErrorBoundTree(math.log(values(0).itemArray.size).toInt, values(0).itemMin, values(0).itemMax, 0.0)
        tree.initTreeArray(values(0).itemMin, values(0).itemMax)
        tree.top2DownUpdateTree(values(0))
        tree.down2TopEstimateErrorBound()
      } else {
        resultStateCompactBitIndexArray = CompactBitIndexArray.combine2CompactBitIndexArray(state.get._1, values(0))
        // (6.2) Update the ErrorBound Tree
        stateErrorBoundTree = state.get._2
        stateErrorBoundTree.top2DownUpdateTree(values(0))
      }
      Some(resultStateCompactBitIndexArray, stateErrorBoundTree)
    })
    // (7) Collect Data And Adaptive Schedule the Input
    var statiscticResult: Array[(String, Array[Int])] = null
    //val collectedDStream = updatedDStream.foreachRDD(rdd => {
    //statiscticResult = rdd.map(row => row).collect()
    //schedulInput(statiscticResult)
    //})
    // (8) Start the stream
    //updatedDStream.print
    reducedDStream.print()
    ssc.start()
    ssc.awaitTermination()
  }

  def schedulInput(statistic: Array[(String, Array[Int])]): Unit ={
  }
}
