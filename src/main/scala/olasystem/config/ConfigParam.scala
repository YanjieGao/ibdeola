package olasystem.config

/**
 * Created by yanjga on 7/13/2015.
 */
object ConfigParam {
  def localconfig(): Unit = {
    System.setProperty("ola.ola.localOrcluster", "local")
    System.setProperty("ola.common.sparkstreaming_batch_seconds", "2")
    System.setProperty("ola.common.spark_master", "local")
    System.setProperty("ola.common.sparkstreaming_batch", "1")
    // spark.shuffle.spill
    System.setProperty("spark.shuffle.spill", "false")
    System.setProperty("ola.ola.TreeDepth", "8")
    System.setProperty("ola.block.block_queue_size", "1000")
    System.setProperty("ola.block.batch_size", "1")
    System.setProperty("ola.block.queue_TrueorFalse", "true")
    ///////////////////////////////////////////////////////////
    System.setProperty("ola.filter.filters_size", "1")
    //////////////////////////////////////////////////////////////
    System.setProperty("ola.ola.ErrorBound", "1000")
    System.setProperty("ola.ola.LowerA", "0")
    System.setProperty("ola.ola.UpperB", "20000")
    System.setProperty("ola.ola.TreeDepth", "3") //Now haven't config this param should rewrite the
    System.setProperty("ola.ola.appduration", "60000")
  }

  def clusterconfig(): Unit = {
    System.setProperty("spark.shuffle.spill", "false")
  }
}