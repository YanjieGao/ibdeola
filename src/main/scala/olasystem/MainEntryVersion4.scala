package olasystem

import olasystem.config.ConfigParam
import olasystem.datasource.{LocalFileReceiver, CustomReceiver}
import olasystem.execute.{ForeachRDDFunction, UpdateFunction, MapFunction}
import olasystem.structure.{ErrorBoundTree, ShuffleCompactArray}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by yanjga on 8/24/2015.
 */
object MainEntryVersion4 {
  def main(args: Array[String]) {
    // (1) Load Data
    ConfigParam.localconfig()
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Seconds(2))
    val customReceiverStream = ssc.receiverStream(new LocalFileReceiver())
    ssc.checkpoint("./checkpointtmp")
    //ssc.checkpoint("D:\\project\\tmpDataFileStore")
    val words = customReceiverStream.map(_.split(" "))
    val kv = words.map(ws =>
      (ws(0), ws(2).toDouble)
    )
    // (2) Build Index
    // (3) Context Init
    // (4) Map Side Aggregation
    val mappedDStream = kv.mapPartitions(iter => {
      MapFunction.mapShuffleCompactFunction(iter)
    })
    // (5) Reduce Side Aggregation and UpdateStateByKey
    val updatedDStream = mappedDStream.updateStateByKey((values: Seq[ShuffleCompactArray], state: Option[(ShuffleCompactArray, ErrorBoundTree)]) => {
      UpdateFunction.combineAndUpdateFunction(values, state)
    })
      // (6) Collect Data And Adaptive Schedule the Input
      .foreachRDD(rdd => {
      ForeachRDDFunction.write2Redis(rdd)
    })
    // launch the stream
    ssc.start()
    ssc.awaitTermination()
  }
}