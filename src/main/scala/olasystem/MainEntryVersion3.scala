package olasystem

import olasystem.config.ConfigParam
import olasystem.datasource.CustomReceiver
import olasystem.execute.{ForeachRDDFunction, UpdateFunction, MapFunction}
import olasystem.structure.{ShuffleCompactArray, ErrorBoundTree, CompactBitIndexArray}
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkContext, SparkConf}
import redis.clients.jedis.{Jedis, JedisPoolConfig, JedisPool}

import scala.collection.mutable

/**
 * Created by yanjga on 7/28/2015.
 */

object MainEntryVersion3 {
  def main(args: Array[String]) {
    // (1) Load Data
    ConfigParam.localconfig()
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Seconds(2))
    val customReceiverStream = ssc.receiverStream(new CustomReceiver())
    ssc.checkpoint("./checkpointtmp")
    //ssc.checkpoint("D:\\project\\tmpDataFileStore")
    //val customReceiverStream = ssc.receiverStream(new FileCustomReceiver())
    val words = customReceiverStream.map(_.split(" "))
    val kv = words.map(ws =>
      (ws(2), ws(4).toDouble)
    )
    // (2) Build Index
    // (3) Context Init
    // (4) Map Side Aggregation
    val mappedDStream = kv.mapPartitions(iter => {
      MapFunction.mapShuffleCompactFunction(iter)
  })
    // (6) UpdateStateByKey
    //    val updatedDStream = reducedDStream.updateStateByKey((values: Seq[ShuffleCompactArray], state: Option[(ShuffleCompactArray, ErrorBoundTree)]) => {
    //      UpdateFunction.updateShuffleFunc(values, state)
    //    })
    val updatedDStream = mappedDStream.updateStateByKey((values: Seq[ShuffleCompactArray], state: Option[(ShuffleCompactArray, ErrorBoundTree)]) => {
      UpdateFunction.combineAndUpdateFunction(values, state)
    })
      .foreachRDD(rdd => {
      ForeachRDDFunction.write2Redis(rdd)
    })
    // (7) Collect Data And Adaptive Schedule the Input
    // (8) Start the stream
    ssc.start()
    ssc.awaitTermination()
  }
}
