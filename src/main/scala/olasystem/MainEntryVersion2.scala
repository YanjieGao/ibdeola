package olasystem

import olasystem.config.ConfigParam
import olasystem.datasource.CustomReceiver
import olasystem.execute.{UpdateFunction, MapFunction, ReduceFunction}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by yanjga on 7/10/2015.
 */
object MainEntryVersion2 {
  def main(args: Array[String]) {
    // (1) Load Data
    ConfigParam.localconfig()
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Seconds(1))
    val customReceiverStream = ssc.receiverStream(new CustomReceiver())
    val words = customReceiverStream.map(_.split(" "))
    val kv = words.map(ws =>
      (ws(2), ws(4).toDouble)
    )
    //kv.print()
    // (2) Build Index

    // (3) Context Init

    // (4) Map Side Aggregation
    val mappedDStream = kv.mapPartitions(iter => {
      MapFunction.mapfunMultiKeys(iter)
    })
    // (5) Reduce Side Aggregation
    import StreamingContext.toPairDStreamFunctions
    val reducedDStream = mappedDStream.reduceByKey((a, b) => ReduceFunction.reduceCombineTree(a, b) )
    // (6) UpdateStateByKey
    val errorBoundTree = Array.fill(System.getProperty("ola.ola.TreeDepth").toInt)(0)
    val updatedDStream = reducedDStream.updateStateByKey((values: Seq[Array[Int]], state: Option[Array[Int]]) => {
      UpdateFunction.updateFunc(values, state, errorBoundTree)
    })
    // (7) Collect Data And Adaptive Schedule the Input
    var statiscticResult: Array[(String, Array[Int])] = null
    //val collectedDStream = updatedDStream.foreachRDD(rdd => {
    //statiscticResult = rdd.map(row => row).collect()
    //schedulInput(statiscticResult)
    //})
    // (8) Start the stream
    reducedDStream.print
    ssc.start()
    ssc.awaitTermination()
  }

  def schedulInput(statistic: Array[(String, Array[Int])]): Unit ={
  }
}
