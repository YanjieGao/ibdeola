/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package olasystem.tran

import org.apache.spark.rdd.RDD
import org.apache.spark.storage.BlockId
import org.apache.spark.{Partition, TaskContext}

import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

/**
 * An RDD that applies the provided function to every partition of the parent RDD.
 */

class TransientEmptyPartition(idx: Int) extends Partition {
  override val index = idx
 // override def hashCode(): Int = 41 * (41 + rddId) + idx // maybe useful in the future
}

class TransientMapPartitionsRDD[T: ClassTag](
    prev: RDD[T]
   )
  extends RDD[T](prev) {

  override val partitioner =  firstParent[T].partitioner

  override def getPartitions: Array[Partition] = firstParent[T].partitions

  override def compute(split: Partition, context: TaskContext): Iterator[T] = {
    val t = firstParent[T].iterator(split, context)
    t
  }
}

class TransientRangeMapPartitionsRDD[T: ClassTag](
                                                   prev: RDD[T],
                                                   start: Int,
                                                   end: Int
                                                   )
  extends RDD[T](prev) {

  override val partitioner = firstParent[T].partitioner

  override def getPartitions: Array[Partition] = getRangePartitions(start, end)

  //  override def getPreferredLocations(split: Partition): Seq[String] = {
  //    // we will add packing logic at this to affect the scheduling mode.
  //    Seq("")
  //  }
  override def compute(split: Partition, context: TaskContext): Iterator[T] = {
    // if no reduce, this rdd will cause parallelRDD exception
    val t = firstParent[T].iterator(split, context)
    t
  }

  def getRangePartitions(start: Int, end: Int): Array[Partition] = {
    // we also need to rename the index of the partitionID, so need new and package new partition, or
    // when repartition the data will lead to errors
    val result = new ArrayBuffer[Partition]
    val iter = firstParent[T].partitions.iterator
    var index = 0
    var newIndex = 0
    while(iter.hasNext) {
      if(start <= index && end >= index) {
        result += new TransientEmptyPartition(newIndex)
        iter.next
        newIndex += 1
          //iter.next()
      } else {
        iter.next()
      }
      index += 1
    }
    result.toArray
  }

  // we could make this as the implicit method, like reduceByKeyMethod
  // This will do a more fine grained split at partition level and key level partition support
  // So this will use two level partition algo(first version we could default implement), and two level reduce algo
  // which should implement by the user
  def secondaryPartitionStrategy(): Unit = {

  }
}
