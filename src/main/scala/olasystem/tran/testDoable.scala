package olasystem.tran

import java.util.concurrent.{Executors, Semaphore}

import olasystem.config.ConfigParam
import olasystem.datasource.LocalFileReceiver
import olasystem.execute.{ForeachRDDFunction, UpdateFunction, MapFunction}
import olasystem.structure.{ErrorBoundTree, ShuffleCompactArray}
import org.apache.spark.scheduler.{SparkListenerTaskStart, SparkListener}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkException, SparkContext, SparkConf}
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent._
import scala.concurrent.duration.Duration

/**
 * Created by yanjga on 11/18/2015.
 */
object testDoable {
  def main(args: Array[String]) {
    // testRangeRDD()
    // testCacheNotExecuteUpStream()
    // testCacheMRExecuteUpStream()
    // testRepartition()
    // testAsyncExecute()
    // testDuplicateTaskSpeed()
    // testMetricsHackingAndLaunchAndKillJob()
    // testJobCancelThroughThreadPool
    //  testCutTheDependencies()
    testCheckpoint()
  }

  def testRangeRDD(): Unit = {
    ConfigParam.localconfig()
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val rdd1 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange = new TransientRangeMapPartitionsRDD(rdd1, 0, 6)
    val c = transientRDDRange.count()
    // use cache to cut the dependencies
    println(c)
  }

  def testCacheNotExecuteUpStream(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val rdd0 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val acc1 = sc.accumulator(0)
    //val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      //acc2 += 1
      item
    })
    //val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange0_6 = new TransientRangeMapPartitionsRDD(rdd1, 0, 6)
    // acc
    val c = transientRDDRange0_6.cache()
    val c1 = c.count()
    val c2 = c.take(1)
    // use cache to cut the dependencies
    println(" c1:" + acc1)
  }

  def testCacheMRExecuteUpStream(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val rdd0 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val acc1 = sc.accumulator(0)
    //val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      //acc2 += 1
      (item, 1)
    }).reduceByKey(_ + _)
    //val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange0_6 = new TransientRangeMapPartitionsRDD(rdd1, 0, 6)
    val transientRDDRange7_10 = new TransientRangeMapPartitionsRDD(rdd1, 7, 10)
    // acc
    val c = transientRDDRange0_6.cache()
    val c1 = c.count()
    val c2 = c.take(1)

    val k = transientRDDRange7_10.cache()
    val k1 = k.count
    val k2 = c.take(1)

    // use cache to cut the dependencies
    println(" c1:" + acc1)
  }

  def testMetricsHackingAndLaunchAndKillJob(): Unit = {

    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext("local[10]", "test")
    // Add a listener to release the semaphore once any tasks are launched.
    // jobA is the one to be cancelled.
    val jobA = future {
      sc.setJobGroup("jobA", "this is a job to be cancelled")
      val a = sc.parallelize(1 to 10000, 1).mapPartitions { i => Thread.sleep(10000000); i }.count()
      println("eA result:" + a)
    }
    // Block until both tasks of job A have started and cancel job A.
    // c.clearJobGroup()
    //val jobB = sc.parallelize(1 to 100, 2).countAsync()
    val jobC = future {
      sc.setJobGroup("jobC", "this is a job to be cancelled")
      val c = sc.parallelize(1 to 10000, 2).count()
      println("eC result:" + c)
    }

    jobA.onComplete {
      case scala.util.Success(res) =>
        println("Should not have reached this code path (onComplete matching Success)")
        throw new Exception("Task should fail")
      case scala.util.Failure(e) =>
        println("failure jobA")
    }

    jobC.onComplete {
      case scala.util.Success(res) =>
        println("Should not have reached this code path (onComplete matching Success)")
        throw new Exception("Task should fail")
      case scala.util.Failure(e) =>
        println("failure jobC")
    }

    val eA = Await.result(jobA, Duration.Inf)
    println("work around the eA")
    //val eC = Await.result(jobC, Duration.Inf)
    println("work around the eC")
    //sc.cancelJobGroup("jobA")
    //println("eA result is:" + eA)
    //println("eC result is" + eC)
  }

  def testStartHookAndEndHook(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext("local[2]", "test")
    // Add a listener to release the semaphore once any tasks are launched.
    // jobA is the one to be cancelled.
    val jobA = future {
      sc.setJobGroup("jobA", "this is a job to be cancelled")
      sc.parallelize(1 to 10000, 2).map { i => Thread.sleep(10); i }.count()
    }
    // Block until both tasks of job A have started and cancel job A.
    sc.clearJobGroup()
    val jobB = sc.parallelize(1 to 100, 2).countAsync()
    sc.cancelJobGroup("jobA")
    val e = Await.result(jobA, Duration.Inf)
  }

  def testRepartition(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext("local[2]", "test")
    // Add a listener to release the semaphore once any tasks are launched.
    // jobA is the one to be cancelled.
    val rdd0 = sc.parallelize(1 to 100, 10)
    // rdd1.count()
    val acc1 = sc.accumulator(0)
    // val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      // acc2 += 1
      (item, 1)
    }).reduceByKey(_ + _).cache
    // val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange0_6 = new TransientRangeMapPartitionsRDD(rdd1, 0, 6).cache()
    val transientRDDRange7_10 = new TransientRangeMapPartitionsRDD(rdd1, 7, 9).cache()

    val jobA = future {
      sc.setJobGroup("jobA", "this is a job to be cancelled")
      val jobACount = transientRDDRange0_6.repartition(200).count
      println("range0_6 count" + jobACount)
    }

    val e1 = Await.result(jobA, Duration.Inf)

    // Block until both tasks of job A have started and cancel job A.
    val jobC = future {
      sc.setJobGroup("jobC", "this is a job to be cancelled")
      val jobCCount = transientRDDRange7_10.repartition(100).count
      //repartition(100).count
      println("range7_10 count" + jobCCount)
    }

    val e2 = Await.result(jobC, Duration.Inf)
    // sc.cancelJobGroup("jobA")
    // sc.clearJobGroup()
  }

  def testAsyncExecute(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext("local[2]", "test")
    // Add a listener to release the semaphore once any tasks are launched.
    // jobA is the one to be cancelled.
    val rdd0 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val acc1 = sc.accumulator(0)
    //val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      //acc2 += 1
      (item, 1)
    }).reduceByKey(_ + _)
    //val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange0_6 = new TransientRangeMapPartitionsRDD(rdd1, 0, 6).cache()
    val transientRDDRange7_10 = new TransientRangeMapPartitionsRDD(rdd1, 7, 10).cache()

    val c1 = transientRDDRange0_6.countAsync()
    val c2 = transientRDDRange7_10.countAsync()

    println("c1" + c1.get())
    println("c2" + c2.get())
  }

  def testDuplicateTaskSpeed(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext("local[2]", "test")
    // Add a listener to release the semaphore once any tasks are launched.
    // jobA is the one to be cancelled.
    val rdd0 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val acc1 = sc.accumulator(0)
    //val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      //acc2 += 1
      (item, 1)
    }).reduceByKey(_ + _).cache
    //val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange0_6 = new TransientRangeMapPartitionsRDD(rdd1, 0, 6).cache()
    val transientRDDRange7_10 = new TransientRangeMapPartitionsRDD(rdd1, 7, 9).cache()
    val jobA = future {
      sc.setJobGroup("jobA", "this is a job to be cancelled")
      val jobACount = transientRDDRange0_6.repartition(200).count
      println("range0_6 count" + jobACount)
    }
    val e1 = Await.result(jobA, Duration.Inf)

    // Block until both tasks of job A have started and cancel job A.
    val jobC = future {
      sc.setJobGroup("jobC", "this is a job to be cancelled")
      val jobCCount = transientRDDRange7_10.repartition(100).count
      //repartition(100).count
      println("range7_10 count" + jobCCount)
    }

    val e2 = Await.result(jobC, Duration.Inf)
  }

  def testJobCancelThroughThreadPool(): Unit = {
    import java.util.concurrent.Callable;
    import java.util.concurrent.ExecutionException;
    import java.util.concurrent.ExecutorService;
    import java.util.concurrent.Executors;
    import java.util.concurrent.Future;

    val es = Executors.newFixedThreadPool(3);


    println("... try to do something while the work is being done....");
    println("... and more ....");

    println("End work" + new java.util.Date());

    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext("local[2]", "test")
    // Add a listener to release the semaphore once any tasks are launched.
    // jobA is the one to be cancelled.
    val rdd0 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val acc1 = sc.accumulator(0)
    //val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      //acc2 += 1
      (item, 1)
    }).reduceByKey(_ + _)
    //val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange0_6 = new TransientRangeMapPartitionsRDD(rdd1, 0, 1).mapPartitions { i => Thread.sleep(10000); i }.cache()
    val transientRDDRange7_10 = new TransientRangeMapPartitionsRDD(rdd1, 7, 10).cache()

    val c2 = transientRDDRange7_10.countAsync()

    val task1 =  new Runnable() {
      override def run(): Unit = {
        sc.setJobGroup("jobA", "this is a job to be cancelled")
        val c1 = transientRDDRange0_6.count
        println("c1:" + c1)
      }
    }

    val future1 =es.submit(task1)

    val task2 =  new Runnable() {
      override def run(): Unit = {
        sc.setJobGroup("jobB", "this is a job to be cancelled")
        val c2 = transientRDDRange7_10.count
        println("c2:" + c2)
      }
    }
    val future2 =es.submit(task2)
    // future1.get(); // blocking call - the main thread blocks until task is done
    // future2.get(); // blocking call - the main thread blocks until task is done
    println("Tasks have finished, this task can be finished")

    val task3 =  new Runnable() {
      override def run(): Unit = {
        Thread.sleep(3000)
        sc.cancelJobGroup("jobA")
        println("cancel jobA")
      }
    }
    val future3 =es.submit(task3)
    es.shutdown()
  }

//  /**
//   * Changes the dependencies of this RDD from its original parents to a new RDD (`newRDD`)
//   * created from the checkpoint file, and forget its old dependencies and partitions.
//   */
//  private[spark] def markCheckpointed(): Unit = {
//    clearDependencies()
//    partitions_ = null
//    deps = null    // Forget the constructor argument for dependencies too
//  }
//
//  /**
//   * Clears the dependencies of this RDD. This method must ensure that all references
//   * to the original parent RDDs is removed to enable the parent RDDs to be garbage
//   * collected. Subclasses of RDD may override this method for implementing their own cleaning
//   * logic. See [[org.apache.spark.rdd.UnionRDD]] for an example.
//   */
//  protected def clearDependencies() {
//    dependencies_ = null
//  }
//  /**
//   * Return the ancestors of the given RDD that are related to it only through a sequence of
//   * narrow dependencies. This traverses the given RDD's dependency tree using DFS, but maintains
//   * no ordering on the RDDs returned.
//   */
//  private[spark] def getNarrowAncestors: Seq[RDD[_]] = {

  def testCutTheDependencies(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)

    val rdd0 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val acc1 = sc.accumulator(0)
    //val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      //acc2 += 1
      (item, 1)
    }).reduceByKey(_ + _)
    //val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange0_6 = new TransientRangeMapPartitionsRDD(rdd1, 0, 6).cache
    val transientRDDRange7_9 = new TransientRangeMapPartitionsRDD(rdd1, 7, 9).cache

    // acc
    val c = transientRDDRange0_6
    val k = transientRDDRange7_9
      //.cache()
    val c1 = c.count()
    val c2 = c.take(1)
    val c3 = c.count
    val k1 = k.count()
    val k2 = k.take(1)
    val k3 = k.count

    rdd1.count
    val test = rdd1.collect

    transientRDDRange0_6.repartition(11).count
    // use cache to cut the dependencies

    // It will auto not find the upstreams
    println(" acc1:" + acc1)
    println("c1:" + c1)
    println("c2:" + c2)
    println("c3:" + c3)

    println("k1:" + k1)
    println("k2:" + k2)
    println("k3:" + k3)
  }

  def testNoCacheAutoNotRetryCommonUpStreams(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)

    val rdd0 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val acc1 = sc.accumulator(0)
    //val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      //acc2 += 1
      (item, 1)
    }).reduceByKey(_ + _)
    //val transientRDD = new TransientMapPartitionsRDD(rdd1)
    val transientRDDRange0_6 = new TransientRangeMapPartitionsRDD(rdd1, 0, 6)
    // acc
    val c = transientRDDRange0_6
    //.cache()
    val c1 = c.count()
    val c2 = c.take(1)
    val c3 = c.count
    // use cache to cut the dependencies
    println(" acc1:" + acc1)
    println("c1:" + c1)
    println("c2:" + c2)
    println("c3:" + c3)
  }

  def testTheDependencies(): Unit = {

  }

  // We can hacking the new DAGScheduler
  // And hacking the new TaskScheduler
  // class FakeDAGScheduler(sc: SparkContext, taskScheduler: FakeTaskScheduler)
  // we can't affect the scheduler to do the packing
  // The fair scheduler also supports grouping jobs into pools, and setting different scheduling
  // options (e.g. weight) for each pool. This can be useful to create a ��high-priority�� pool for more important jobs,
  // spark.scheduler.pool
  // So this version can't support packing the task to the node.
  // Could encode the executor at the queue? or just the priority of the queue. If we could fix the executor to specific
  // queue, then we could control the scheduling and packing job to specific nodes.
  // Now we can only control the priority of each job
  // We can throgh redefine the preferred locations of RDD to affect the locality and location

  // (1) Affect the pool weight to affect the priority of the job
  // (2) Redefine at the new RDD preferred locations to affect each task locations of packing the stage.
  // (3) When not packing we could use task repartition and submit new tasks to split the longing tasks
  // Why not for packing to hacking the kernal of Spark
  // (1) The time estimate is not accurate
  // (2) No one want to use a new version of Spark except the inner team maintain the services, like azure of DM.
  // (3) Upper project could open source.

  def testThePackingProblem(): Unit = {

  }

  def testCheckpoint(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)

    val rdd0 = sc.parallelize(1 to 100, 10)
    //rdd1.count()
    val acc1 = sc.accumulator(0)
    //val acc2 = sc.accumulator(0)
    val rdd1 = rdd0.map(item => {
      acc1 += 1
      //acc2 += 1
      (item, 1)
    }).reduceByKey(_ + _)
    //val transientRDD = new TransientM

    val c = rdd1.count

    println("test Job has finished")
    println("result is "  + c)
  }

}
