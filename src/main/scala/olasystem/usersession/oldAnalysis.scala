//package olasystem.usersession
//
///**
// * Created by yanjga on 10/26/2015.
// */
//class oldAnalysis {
//
//}
//def analysisPast {
//val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
//val sc = new SparkContext(conf)
//val sqlContext = new org.apache.spark.sql.SQLContext(sc)
//import sqlContext.implicits._
//// Parse the log, assume query 6 columns from the raw log, this can be extended
//val text = sc.textFile("D:\\dataset\\UserSession.txt").map(_.split("\t")).map(data => {
//val dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
//if (data.size < 5) {
//userLog(new java.sql.Timestamp(dateFormat.parse(data(0)).getTime), data(1), data(2), data(3), "", "")
//} else {
//userLog(new java.sql.Timestamp(dateFormat.parse(data(0)).getTime), data(1), data(2), data(3), data(4), data(5))
//}
//})
//text.toDF.registerTempTable("src")
//
//val userData = sqlContext.sql("select * from src")
//
//// Aggregation each run session's start time
//val sessionStartTimeMap: collection.Map[(String, String), Timestamp] = text.filter((item: userLog) => {
//item.Name == "Run"
//}).map(item => {
//((item.NotebookId, item.ParagraphId), item.OccurredTimestamp)
//}).reduceByKey((a: Timestamp, b: Timestamp) => {
//if (a.before(b)) {
//a
//} else {
//b
//}
//}).collectAsMap()
//
//// broadcast the start time map
//val startTimeMap = sc.broadcast(sessionStartTimeMap)
//val resultLog = text.map(item => {
//(item.NotebookId, item)
//})
//// GroupBy each notebook
//.groupByKey()
//// sort run log and view log seq based on timestamp, then iteration traversal the run log, tag the view based on the nearest run log paragraphId
//.mapPartitions((noteBookIter: Iterator[(String, Iterable[userLog])]) => {
//val newIter = noteBookIter.flatMap((item: (String, Iterable[userLog])) => {
//val data = item._2.toSeq
//// sort the view log based on timestamp
//val sortedViewData = data.filter((i: userLog) => i.Name == "View").sortBy((j: userLog) => j.OccurredTimestamp.getTime).reverse
//// map the run log and generate new log
//val runData = data.filter((i: userLog) => i.Name != "View")
//.map((log: userLog) => new newLog(log, log.ParagraphId, log.OccurredTimestamp))
//// sort this notebook's run log map
//val sortedRunDataMap = startTimeMap.value.filter((para: ((String, String), Timestamp)) => para._1._1 == item._1)
//.map((para: ((String, String), Timestamp)) => (para._1._2, para._2)).toSeq
//.sortBy((p: (String, Timestamp)) => p._2.getTime).reverse
//val runIter = sortedRunDataMap.iterator
//val viewIter = sortedViewData.iterator
//val viewBuffer = new ArrayBuffer[newLog]
//var flag = true
//var viewFlag = true
//// iteration traversal the run log, tag the view based on the nearest run log paragraphId
//while (runIter.hasNext && flag) {
//val tmpRun = runIter.next
//while (viewIter.hasNext && viewFlag) {
//val viewData = viewIter.next()
//if (viewData.OccurredTimestamp.before(tmpRun._2)) {
//// view data can be tagged by the run log
//viewFlag = false
//viewBuffer += new newLog(viewData, tmpRun._1, tmpRun._2)
//} else {
//// If view data can't be tagged by the run log
//viewBuffer += new newLog(viewData, "", viewData.OccurredTimestamp)
//}
//}
//viewFlag = true
//if (!viewIter.hasNext) {
//flag = false
//}
//}
//while(viewIter.hasNext) {
//var viewLog = viewIter.next()
//viewBuffer += new newLog(viewLog, "", viewLog.OccurredTimestamp)
//}
//val result = viewBuffer.union(runData)
//result
//})
//newIter
//})
//
//val collectedData = resultLog.collect()
//val printableData = collectedData.sortBy((item: newLog) => item.log.OccurredTimestamp.getTime).map((logData: newLog) => {
//logData.log.NotebookId + "\t" +logData.log.Name + "\t" + logData.log.OccurredTimestamp + "\t" + logData.sessionId + "\t" //+ logData.startTime
//})
//
//val taggedViewNumbers = collectedData.filter((item: newLog) => {
//item.log.Name == "View" && item.sessionId != ""
//}).size
//
//val distinctSessionID = collectedData.filter((item: newLog) => {
//item.log.Name == "Run"
//}).map((item: newLog) => {
//item.log.ParagraphId
//}).distinct.size
//
//val sortedMap = new java.util.HashMap[String,(Long, ArrayBuffer[newLog])]
//collectedData.foreach(item => {
//if(sortedMap.containsKey(item.sessionId)) {
//val time = Math.min(item.log.OccurredTimestamp.getTime, sortedMap.get(item.sessionId)._1)
//sortedMap.get(item.sessionId)._2 += item
//sortedMap.put(item.sessionId, (time , sortedMap.get(item.sessionId)._2))
//} else {
//sortedMap.put(item.sessionId, (0, new ArrayBuffer[newLog]))
//sortedMap.get(item.sessionId)._2 += item
//sortedMap.put(item.sessionId, (item.log.OccurredTimestamp.getTime , sortedMap.get(item.sessionId)._2))
//}
//})
//import scala.collection.JavaConverters._
//val result = sortedMap.asScala.map(item => {
//item._2._2.sortBy(i => i.log.OccurredTimestamp.getTime)
//(item._2)
//}).toSeq.sortBy(i => i._1).iterator
//
//while (result.hasNext)
//{
//val tmp = result.next()
//println("----------------------------------------------------------------")
//val iter = tmp._2.sortBy(_.log.OccurredTimestamp.getTime).map((logData: newLog) => {
//logData.log.NotebookId + "\t" +logData.log.RawUserData + "\t" + logData.log.Name + "\t" +
//logData.log.OccurredTimestamp + "\t" + logData.sessionId + "\t" + logData.log.ParagraphId + "\t" + logData.log.ParagraphTyp
//}).iterator
//while(iter.hasNext) {
//println("NotebookId RawUserData Name OccurredTimestamp sessionId ParagraphId ParagraphTyp")
//println(iter.next())
//}
//}
//println("taggedViewNumbers:" + taggedViewNumbers + "  distinctSessionID:" + distinctSessionID)
//}
//
//case class subResultLogSchema()
//def analysisResult(): Unit = {
//val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
//val sc = new SparkContext(conf)
//val sqlContext = new org.apache.spark.sql.SQLContext(sc)
//import sqlContext.implicits._
//// Parse the log, assume query 6 columns from the raw log, this can be extended
//val text = sc.textFile("D:\\dataset\\UserSessionSubResult.txt").map(item => {
//val s = item.split("\t")
//s
//})
//val sessionDuration = 15 // s
//// get the time duration and partition the data rows
//val noteBookStartTime = text.map(item => item(0).toLong).min()
//val noteBookStopTime = text.map(item => item(0).toLong).max()
//
//val createActionSet = text.filter(item => item(3) == "create").collect()
//val postCreateSessionSet = createActionSet.map(item => {
//((item(0).toLong - noteBookStartTime) / sessionDuration) + 1
//}).toSet
//val startActionSet = text.filter(item => item(3) == "start").collect
//val postStartSessionMap = startActionSet.map(item => {
//val sessionID = ((item(0).toLong - noteBookStartTime) / sessionDuration) + 1
//(sessionID, item(0).toLong)
//}).toMap
//text.map(item => {
//val sessionIDOfNoteBook = (item(0).toLong - noteBookStartTime) / sessionDuration
//(sessionIDOfNoteBook, item)
//}).groupByKey().map(item => {
//val sessionData = item._2.toArray
//val NoteBookID = sessionData(0)(2)
//val SessionStartDateTime = sessionData.map(item => item(0)).min
//val SessionEndDateTime = sessionData.map(item => item(0)).max
//
//val isPostCreate = postCreateSessionSet.contains(item._1)
//val isPostStart = postStartSessionMap.contains(item._1)
//var StartWaitingTime: Long = 0L
//
//if(isPostStart) {
//StartWaitingTime = postStartSessionMap.get(item._1).get
//}
//val NbOfParagraphRun = sessionData.size
//(NoteBookID, SessionStartDateTime, SessionEndDateTime, isPostCreate, isPostStart, StartWaitingTime, NbOfParagraphRun)
//})
//
//text.take(11).foreach(println(_))
//}
//
//}
