package olasystem.usersession

import java.io.File
import java.text.SimpleDateFormat
import org.apache.spark.{SparkContext, SparkConf}
import java.sql.{Date, Timestamp}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
/**
 * Created by yanjga on 9/15/2015.
 */
case class userLog(OccurredTimestamp: Timestamp, RawUserData: String, NotebookId: String, Name: String, ParagraphTyp: String, ParagraphId: String)

class newLog(logT: userLog, sessionIdT: String, startTimeT: Timestamp) extends Serializable {
  var log: userLog = logT
  var sessionId = sessionIdT
  //var startTime = startTimeT
}

object UserSessionAnalysis_OutputMultiColumnResult {
  def main(args: Array[String]): Unit = {
    analysisSubResult()
  }

  def analysisSubResult(): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._
    // Parse the log, assume query 6 columns from the raw log, this can be extended
    // log query sql is:sql("select OccurredTimestamp, RawUserData, NotebookId, Name, rawname, ParagraphType, ParagraphId from UserActions_1  where OccurredTimestamp > '2015-09-01 00:50:00.0'  order by NotebookId, OccurredTimestamp asc ")
    val text = sc.textFile("D:\\dataset\\SparkSvcZeppelinUserActions.csv").map(_.split(",")).map(data => {
      val dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
      if (data.size <= 5) {
        userLog(new java.sql.Timestamp(dateFormat.parse(data(0)).getTime), data(1), data(2), data(4), "", "")
      } else {
        if (data.size <= 5) {
          /// println()
        }
        userLog(new java.sql.Timestamp(dateFormat.parse(data(0)).getTime), data(1), data(2), data(4), data(5), data(6))
      }
    })
    val sessionDuration = 15 * 60 * 1000 // 15 minutes, java.sql.type use milliseconds

    val result = text.groupBy(_.NotebookId).map(group => {
      val groupData = group._2.toSeq
      val noteBookID = group._1
      val noteBookStratTime = groupData.map(_.OccurredTimestamp.getTime).min
      var sessionIDNumber = 0L
      var sorted = groupData.sortBy(log => log.OccurredTimestamp.getTime)
      var prev: userLog = null
      var current: userLog = null
      var iter = sorted.iterator
      val sessionedResult = new mutable.HashMap[Long, ArrayBuffer[userLog]]
      while (iter.hasNext) {
        if (prev == null) {
          var tmp = iter.next()
          sessionedResult += (0L -> new ArrayBuffer[userLog])
          prev = tmp
          //sessionedResult(0) += tmp
        } else {
          var cur = iter.next()
          if (sessionedResult.get(sessionIDNumber).isEmpty) {
            sessionedResult += (sessionIDNumber -> new ArrayBuffer[userLog])
          }
          sessionedResult(sessionIDNumber) += prev
          if (cur.OccurredTimestamp.getTime - prev.OccurredTimestamp.getTime >= sessionDuration) {
            sessionIDNumber += 1
          }
          prev = cur
        }
      }
      if (sessionedResult.get(sessionIDNumber).isEmpty) {
        sessionedResult += (sessionIDNumber -> new ArrayBuffer[userLog])
      }
      sessionedResult(sessionIDNumber) += prev

      val sessionedData = sessionedResult.map(item => {
        (item._1, item._2.map(row => (item._1, row)).toSeq)
      }).toMap

      val createdMap = sessionedData.flatMap(_._2).filter(_._2.Name == "Create")
        .map(row =>
        ((row._1), row._2.OccurredTimestamp.getTime)).toMap
      val startedMap = sessionedData.flatMap(_._2).filter(_._2.Name == "Start")
        .map(row => ((row._1), row._2.OccurredTimestamp.getTime)).toMap
      if (startedMap.size > 5) {
        println()
      }

      val resultS = sessionedData.map(item => {
        val sessionIDOfNotebook = item._1
        val sessionGroupData = item._2
        val sessionStartTime = sessionGroupData.minBy(_._2.OccurredTimestamp.getTime)
        val sessionEndTime = sessionGroupData.maxBy(_._2.OccurredTimestamp.getTime)
        val NbOfParagraphRun = sessionGroupData.size
        val isPostCreate = (createdMap.contains(item._1))
        val isPostStart = (startedMap.contains(item._1))
        var StartWaitingTime = -1L
        if (startedMap.contains(item._1)) {
          val StartWaitingTimeSub = sessionGroupData.filter(row => {
            val time = row._2.OccurredTimestamp.getTime - startedMap.get(row._1).get
            time >= 0
          }).map(row => {
            val time = row._2.OccurredTimestamp.getTime - startedMap.get(row._1).get
            time.toLong
          })
          if (!StartWaitingTimeSub.isEmpty) {
            StartWaitingTime = StartWaitingTimeSub.min / 1000 * 1000
          }
        }
        (noteBookID, sessionIDOfNotebook, sessionStartTime._2.OccurredTimestamp.toString, sessionEndTime._2.OccurredTimestamp.toString, isPostCreate, isPostStart, StartWaitingTime, NbOfParagraphRun)
      })
      resultS
    })

    val pw = new java.io.PrintWriter(new File("D:\\dataset\\result.txt"))
    val c = result.map(row => {
      val s = row.toSeq.sortBy(_._2)
      s
    }).flatMap(r => r).collect
    c.foreach(r => pw.println(r.toString.replace("(", "").replace(")", "")))
    pw.close()
  }
}