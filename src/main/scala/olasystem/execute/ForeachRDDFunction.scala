package olasystem.execute

import olasystem.structure.{ErrorBoundTree, ShuffleCompactArray}
import org.apache.spark.rdd.RDD
import redis.clients.jedis.{Jedis, JedisPoolConfig, JedisPool}

/**
 * Created by yanjga on 8/6/2015.
 */
object ForeachRDDFunction {
  def main(args: Array[String]) {

  }

  val write2Redis = (rdd: RDD[(String, (ShuffleCompactArray, ErrorBoundTree))]) => {
    val sample = rdd.mapPartitions(iter => {
      // test Redis config
      val pool: JedisPool = new JedisPool(new JedisPoolConfig, "localhost")
      /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
      var jedis: Jedis = null
      try {
        jedis = pool.getResource
        while(iter.hasNext) {
          var tmpItem = iter.next()
          var bitIndex = tmpItem._2._2.bitMarkedIndex
          var bitString = tmpItem._2._1.transformBitSet2String(tmpItem._2._2.bitMarkedIndex)
          jedis.set(tmpItem._1, bitString)
        }
      } finally {
        pool.destroy
      }
      iter
    }).take(11)
    sample.foreach(item => {
      println(item)
    })
  }
}
