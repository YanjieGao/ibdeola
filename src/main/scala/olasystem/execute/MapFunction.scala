package olasystem.execute

import java.util

import olasystem.structure._
import org.apache.spark.SparkException
import redis.clients.jedis.{Jedis, JedisPool, JedisPoolConfig}

import scala.collection.mutable

/**
 * Created by yanjga on 7/10/2015.
 */
object MapFunction {
  def mapfun(iter: Iterator[(String, Double)]): Iterator[(String, Array[Int])] = {
    //The tree array
    println("enter MapFunction's mapfun")
    val treeDepth = System.getProperty("ola.ola.TreeDepth").toInt
    //1*1 - 2^(k - 1))/ ( -1),should be math.pow(2,treeDepth ).toInt - 1,but use 0 as null so usemath.pow(2,treeDepth ).toInt space
    val treenodenum = math.pow(2, treeDepth).toInt
    var tree = new Array[Int](treenodenum + 1) //+1 because the 0 index didn't use
    var bucketnum = math.pow(2, treeDepth - 1).toInt
    var tmp = 0.0
    var bucketIndex = 0
    var treeIndex = 0
    var bucketOffset = math.abs((System.getProperty("ola.ola.UpperB").toInt - System.getProperty("ola.ola.LowerA").toInt) / bucketnum)
    // update the leaf node to the root node
    var upperBound = System.getProperty("ola.ola.UpperB").toDouble
    var sum = 0.0
    var count = 0
    var treeLevelStart = math.pow(2, treeDepth - 1)
    val endTime1 = System.currentTimeMillis()
    //The code here is very easy to over the tree index , throw the arrayoutofbound Exception
    var tmpIndex = 0
    while (iter.hasNext) {
      // tmp may contains negative num , so the bucket should redesign ,but this time we transfer data to positive
      tmp = iter.next()._2
      sum += tmp
      count += 1
      bucketIndex = (tmp / bucketOffset).toInt
      tmpIndex = (treeLevelStart + bucketIndex).toInt
      if ((tmpIndex) > treenodenum) {
        treeIndex = treenodenum
      } else {
        treeIndex = tmpIndex
      }
      //check to avoid the index out of the index of the tree Array
      //should remove this if in future
      tree(treeIndex) += 1
    }
    tree(0) = (sum / count).toInt
    Seq(("1", tree)).iterator
  }

  def mapfunMultiKeys(iter: Iterator[(String, Double)]): Iterator[(String, Array[Int])] = {
    val result = new mutable.HashMap[String, Array[Int]]
    var tmpItem: (String, Double) = null
    val treeDepth = System.getProperty("ola.ola.TreeDepth").toInt
    while(iter.hasNext) {
      tmpItem = iter.next()
      //assignValue2Tree(result.getOrElse(tmpItem._1, Array.fill(treeDepth)(0)), tmpItem._2)
    }
    result.iterator
  }

  def mapfunUseSkipList(iter: Iterator[(String, Double)]): Iterator[(String, CompactBitIndexArray)] = {
    // indexCount: Int, listItemCount: Int, rMin: Double, rMax: Double
    val seq = iter.toArray
    val indexCount = 4
    val listItemCount = 8
    val metaData = new mutable.HashMap[String, statisticOfTheKey]()
    // occur concurrent problem
//    var iterA = seq.iterator
//    while(iterA.hasNext) {
//      val item = iterA.next()
//      // These value can config
//      if (!metaData.contains(item._1)) {
//        metaData.put(item._1, new statisticOfTheKey)
//      } else {
//        metaData(item._1).indexCount = indexCount
//        metaData(item._1).listItemCount = listItemCount
//        metaData(item._1).rMin = math.min(metaData(item._1).rMin, item._2)
//        metaData(item._1).rMax = math.max(metaData(item._1).rMax, item._2)
//      }
//    }
//    val resultMap = new mutable.HashMap[String, MapSkipList]()
//    iterA = seq.iterator
//    while(iterA.hasNext) {
//      val item = iterA.next()
//      if (resultMap.contains(item._1)) {
//        resultMap(item._1).insertItem(item._2)
//      } else {
//        val skipList = new MapSkipList(metaData(item._1))
//        resultMap.put(item._1, skipList)
//      }
//    }
    seq.foreach(item => {
      // These value can config
      if (!metaData.contains(item._1)) {
        metaData.put(item._1, new statisticOfTheKey)
      } else {
        metaData(item._1).indexCount = indexCount
        metaData(item._1).listItemCount = listItemCount
        metaData(item._1).rMin = math.min(metaData(item._1).rMin, item._2)
        metaData(item._1).rMax = math.max(metaData(item._1).rMax, item._2)
      }
    })

    val resultMap = new mutable.HashMap[String, MapSkipList]()
    seq.foreach(item => {
      if (resultMap.contains(item._1)) {
        resultMap(item._1).insertItem(item._2)
      } else {
        val skipList = new MapSkipList(metaData(item._1))
        resultMap.put(item._1, skipList)
      }
    })

    val compactSet = resultMap.map(item => {
      val compact = new CompactBitIndexArray(item._2.linkedItemCapacity, item._2.rangMin, item._2.rangeMax)
      compact.transferMapSkipList2CompactArray(item._2)
      (item._1, compact)
    })
    compactSet.iterator
    }

//  def mapfunUseBitCompress(iter: Iterator[(String, Double)]): Iterator[(String, ShuffleCompactArray)] = {
//    // Use Redis get the Bit Array
//    //val pool = new JedisPool(new JedisPoolConfig(), "localhost")
//    // Config Param
//    val arraySize = 2^8
//    val needSum = false
//    var minC = 0.0
//    var maxC = 10000.0
//    var rangeIndexC = new Array[Short](arraySize)
//    // Config Param
//    val dataAgg = new mutable.HashMap[String, ShuffleCompactArray]()
//    var tmpKey: (String, Double) = null
//    while(iter.hasNext) {
//      tmpKey = iter.next()
//      if(!dataAgg.contains(tmpKey._1)) {
//        dataAgg.put(tmpKey._1, new ShuffleCompactArray(arraySize, needSum, minC, maxC, rangeIndexC))
//      }
//      dataAgg(tmpKey._1).accumulateItem(tmpKey._2)
//    }
//    dataAgg.iterator
//  }

  def generateBitString(bitLength: Int, start: Int): String = {
    var result = new StringBuffer()
    var i = 0
    while(i < bitLength) {
      if(i >= start) {
        result.append("1")
      } else {
        result.append("0")
      }
      i += 1
    }
    result.toString
  }

  def mapShuffleCompactFunction(iter: Iterator[(String, Double)]): Iterator[(String, ShuffleCompactArray)] = {
    // (1)Read Redis to decide the tree structure
    val pool: JedisPool = new JedisPool(new JedisPoolConfig, "localhost")
    /// Jedis implements Closable. Hence, the jedis instance will be auto-closed after the last statement.
    var jedis: Jedis = null
    // the array first cell is 0, 00001111
    var treeDepth = 3
    var treeLeafNodesNumber = math.pow(2, (treeDepth - 1)).toInt
    var treeNodesNumber = math.pow(2, treeDepth).toInt - 1
    try {
      jedis = pool.getResource
      // (2)Do the map function
      val arraySize = treeNodesNumber + 1
      val needSum = false
      var minC = 0.0
      var maxC = 100.0
      var rangeIndexC = new Array[Short](arraySize)
      var tmpTreeStructure: String = ""
      // Config Param
      val dataAgg = new mutable.HashMap[String, ShuffleCompactArray]()
      var tmpKey: (String, Double) = null
      while (iter.hasNext) {
        tmpKey = iter.next()
        if (!dataAgg.contains(tmpKey._1)) {
          // (3) new the tree structure based on jedis meta data
          tmpTreeStructure = jedis.get(tmpKey._1)
          var shuffleCompactArray = new ShuffleCompactArray(arraySize, needSum, minC, maxC, rangeIndexC)
          if(tmpTreeStructure == null) {
            shuffleCompactArray.compressTreeSchema = generateBitString(treeNodesNumber + 1, treeNodesNumber + 1 - treeLeafNodesNumber)
          } else {
            shuffleCompactArray.compressTreeSchema = tmpTreeStructure
          }
          dataAgg.put(tmpKey._1, shuffleCompactArray)
        }
        dataAgg(tmpKey._1).accumulateItem(tmpKey._2)
      }

      dataAgg.map(item => {
        if (item._2.compressTreeSchema != null) {
          item._2.CompressDataUseBitSet()
        }
        item
      })
      //   dataAgg.iterator
      //   dataAgg(tmpKey._1).accumulateItem(tmpKey._2)
      //   dataAgg.foreach(item => {
      //   item._2.CompressDataStore()
      // })
      dataAgg.iterator
    }
    catch {
      case e: Exception =>
        println(e.toString)
        throw new SparkException(e.toString, e)
    }
    finally {
      pool.destroy
    }
  }
}

