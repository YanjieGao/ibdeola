package olasystem.execute

import olasystem.structure.{ErrorBoundTree, ShuffleCompactArray}
import org.apache.spark.SparkEnv

/**
 * Created by yanjga on 7/10/2015.
 */
object UpdateFunction {
  val updateFunc = (values: Seq[Array[Int]], state: Option[Array[Int]], errorboundTree: Array[Int]) => {
    val startTime = System.currentTimeMillis()
    println("enter updateFunc")
    val env = SparkEnv.get
    val ErrorNumTree = errorboundTree
    // combine different RDD's ErrorBound
    var prev = state.getOrElse(null)
    val count = values.length
    val ErrorBound = System.getProperty("ola.ola.ErrorBound").toDouble
    var start = 0
    val treeDepth = System.getProperty("ola.ola.TreeDepth").toInt
    //1 *�?1 - 2^(k - 1))/ ( -1),should be math.pow(2,treeDepth ).toInt - 1,but use 0 as null so usemath.pow(2,treeDepth ).toInt space
    val treenodenum = math.pow(2, treeDepth - 1).toInt
    val zeroTree = Array.fill(state.size)(0)
    val valuecombine = values.foldLeft(zeroTree)(ReduceFunction.reduceCombineTree)
    if (prev == null) Some(valuecombine)
    else {
      var combine = ReduceFunction.reduceCombineTree(prev, valuecombine)
      val merged = mergeTree(combine, ErrorNumTree)
      if (merged != null && merged(1) > ErrorNumTree(1)) {
      } else {
      }
      val endTime = System.currentTimeMillis()
      println("updateFun cost time" + (endTime - startTime) / 1000 + "seconds")
      println("merged root num" + merged(1))
      Some(merged)
    }
  }

  def mergeTree(tree: Array[Int], errorNumTree: Array[Int]): Array[Int] = {
    val startTime = System.currentTimeMillis()
    if (tree == null) return null
    var i = tree.size - 2
    var j = 0
    println("////////////////")
    println("treesize" + tree.size)
    println("errorNumTreesize" + errorNumTree.size)
    println("////////////")
    while (i > 0) {
      if (tree(i) > errorNumTree(i)) {
        // if it is the left tree node
        if (i % 2 == 0) {
          if (tree(i + 1) > errorNumTree(i + 1)) {
            // if the two child (left and right reach the num then update the parent reach the error)
            tree(i / 2) = errorNumTree(i / 2) + 1
          }
        }
      }
      i -= 1
    }
    val endTime = System.currentTimeMillis()
    println("mergeTree cost time" + (endTime - startTime) / 1000 + "seconds")
    tree
  }

  val updateShuffleFunc = (values: Seq[ShuffleCompactArray], state: Option[(ShuffleCompactArray, ErrorBoundTree)]) => {
    // (6.1) Aggregate the new value with the previous value
    var resultStateShuffleArray: ShuffleCompactArray = null
    var stateErrorBoundTree: ErrorBoundTree = null
    var old: (ShuffleCompactArray, ErrorBoundTree) = null
    if (state.isEmpty) {
      if (values.size > 0) {
        //val deptest = math.log(16) / math.log(2)
        var treeDepth = (math.log(values(0).compactDataCount.size) / math.log(2)).toInt
        if (treeDepth <= 1) {
          print()
        }
        val tree = new ErrorBoundTree(treeDepth, values(0).min, values(0).max, 0.0)
        tree.initTreeArray(values(0).min, values(0).max)
        tree.top2DownUpdateTreeShuffleCompact(values(0))
        tree.down2TopEstimateErrorBound()
        stateErrorBoundTree = tree
        resultStateShuffleArray = values(0)
      } else {
        state
      }
    } else {
      old = state.get
      //resultStateCompactBitIndexArray = CompactBitIndexArray.combine2CompactBitIndexArray(state.get._1, values(0))
      // (6.2) Update the ErrorBound Tree
      stateErrorBoundTree = state.get._2
      stateErrorBoundTree.top2DownUpdateTreeShuffleCompact(values(0))
      old._1.combine(values(0))
      resultStateShuffleArray = old._1
    }
    // Use Redis store the state, tablename_keyname value store the result, this will fast query and update the value and don't need whole table scan the data
    //
    Some(resultStateShuffleArray, stateErrorBoundTree)
  }

  val combineAndUpdateFunction =  (values: Seq[ShuffleCompactArray], state: Option[(ShuffleCompactArray, ErrorBoundTree)]) => {
    //var resultStateShuffleArray: ShuffleCompactArray = null
    //var stateErrorBoundTree: ErrorBoundTree = null
    var old: (ShuffleCompactArray, ErrorBoundTree) = null
    var baseArray: ShuffleCompactArray = null
    var tree: ErrorBoundTree = null
    if (values.size > 0) {
      val firstArray = values(0)
      val mostGeneralIndex = firstArray.findMostGeneralBitIndex(values)
      if (state.isEmpty) {
        baseArray = new ShuffleCompactArray(firstArray.treeNodesCount + 1, firstArray.needSumFlag, firstArray.min, firstArray.max, firstArray.rangeIndex)
        baseArray.compressTreeSchema = firstArray.transformBitSet2String(mostGeneralIndex)
        baseArray.bitSetTreeSchema = mostGeneralIndex
        var treeDepth = (math.log(baseArray.treeNodesCount + 1) / math.log(2)).toInt
        tree = new ErrorBoundTree(treeDepth, baseArray.min, baseArray.max, 3.0)
        tree.initTreeArray(baseArray.min, baseArray.max)
      } else {
        baseArray = state.get._1
        tree = state.get._2
      }
      val (bitSetTree, newArray) = baseArray.combineMultiRangeBitSetAndArray(baseArray.bitSetTreeSchema, values, baseArray)
      // After combine the tree node is the not stop leaf nodes.
      tree.top2DownUpdateTreeShuffleCompact(baseArray)
      tree.down2TopEstimateErrorBound()
      val flag = baseArray.transformBitSet2String(tree.bitMarkedIndex)
      baseArray.bitSetTreeSchema = tree.bitMarkedIndex
      baseArray.compressTreeSchema = flag
      //stateErrorBoundTree = tree
     // resultStateShuffleArray = baseArray
    } else {
      state
    }
    // Use Redis store the state, tablename_keyname value store the result, this will fast query and update the value and don't need whole table scan the data
    Some(baseArray, tree)
  }
}
