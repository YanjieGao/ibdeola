package olasystem.execute

/**
 * Created by yanjga on 7/10/2015.
 */
object ReduceFunction {
  def reduceCombineTree(left: Array[Int], right: Array[Int]): Array[Int] = {
    //val startTime = System.currentTimeMillis()
    var tmp : Array[Int] = null
    if(right.size > left.size) {
      if (left == null) return null
      else if (right == null) return left
      var Index = left.size - 1
      while (Index > 0) {
        right(Index) += left(Index)
        Index -= 1
      }
      //do the agg function
      right(0) = AggTwo(left(0), right(0)).toInt
      right
    } else {
      if (left == null) return null
      else if (right == null) return left
      var Index = right.size - 1
      while (Index > 0) {
        left(Index) += right(Index)
        Index -= 1
      }
      //do the agg function
      left(0) = AggTwo(left(0), right(0)).toInt
      //val endTime = System.currentTimeMillis()
      //println("reduceCombineTree cost time" + (endTime - startTime) / 1000 + "seconds")
      left
    }
  }

  def AggTwo(a: Double, b: Double): Double = {
    return (a + b) / 2
  }

  def reduceCombineTreeMultiKeys(left: Array[Int], right: Array[Int]): Array[Int] = {
    //val startTime = System.currentTimeMillis()
    var tmp : Array[Int] = null
    var treeLength = 0
        if(right.size > left.size) {
          if (left == null) return null
          else if (right == null) return left
          var Index = left.size - 1
          while (Index > 0) {
            right(Index) += left(Index)
            Index -= 1
          }
          //do the agg function
          right(0) = AggTwo(left(0), right(0)).toInt
          right
        } else {
          if (left == null) return null
          else if (right == null) return left
          var Index = right.size - 1
          while (Index > 0) {
            left(Index) += right(Index)
        Index -= 1
      }
      //do the agg function
      left(0) = AggTwo(left(0), right(0)).toInt
      //val endTime = System.currentTimeMillis()
      //println("reduceCombineTree cost time" + (endTime - startTime) / 1000 + "seconds")
      left
    }
  }

}
