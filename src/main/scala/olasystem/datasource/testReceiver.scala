package olasystem.datasource

/**
 * Created by yanjga on 7/10/2015.
 */
import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._ // not necessary in Spark 1.3+
object testReceiver {
  def main(args: Array[String]) {
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Seconds(1))
    val customReceiverStream = ssc.receiverStream(new CustomReceiver())
    val words = customReceiverStream.flatMap(_.split(" "))
    words.print()
    ssc.start()
    ssc.awaitTermination()
  }
}
