package olasystem.datasource

import org.apache.spark.storage.StorageLevel
import java.io.{InputStreamReader, BufferedReader, InputStream}
import java.net.Socket

import org.apache.spark.{SparkConf, Logging}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.receiver.Receiver
/**
 * Created by yanjga on 7/10/2015.
 */

class CustomReceiver()
  extends Receiver[String](StorageLevel.MEMORY_AND_DISK_2) with Logging {

  def onStart() {
    // Start the thread that receives data over a connection
    new Thread("Socket Receiver") {
      override def run() {
        receive()
      }
    }.start()
  }

  def onStop() {
    // There is nothing much to do as the thread calling receive()
    // is designed to stop by itself isStopped() returns false
  }

  /** Create a socket connection and receive data until receiver is stopped */
  private def receive() {
    println("Receiver receive logs")
    // var socket: Socket = null
    // var userInput: String = null
    try {
      // Connect to host:port
      //  socket = new Socket(host, port)

      // Until stopped or connection broken continue reading
      //   val reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"))
      //   userInput = reader.readLine()
      //   while(!isStopped && userInput != null) {
      while (!isStopped) {
        //     store(userInput)
        store(LogGenerator.generateLog())
        //      userInput = reader.readLine()
      }
      // reader.close()
      // socket.close()

      // Restart in an attempt to connect again when server is active again
      restart("Trying to connect again")
    } catch {
      case e: java.net.ConnectException =>
        // restart if could not connect to server
        restart("Error Start Customer Receiver")
      case t: Throwable =>
        // restart if there is any other error
        restart("Error receiving data", t)
    }
  }
}