package olasystem.datasource

import org.apache.spark.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver
import scala.io.Source._
/**
 * Created by yanjga on 7/15/2015.
 */

class FileCustomReceiver()
  extends Receiver[String](StorageLevel.MEMORY_AND_DISK_2) with Logging {

  val file = fromFile("D:\\dataset\\amazon\\pagecounts-20150630-230000\\pagecounts-20150630-230000")
  val lines = file.getLines
  def onStart() {
    // Start the thread that receives data over a connection
    new Thread("Socket Receiver") {
      override def run() {
        receive()
      }
    }.start()
  }

  def onStop() {
    // There is nothing much to do as the thread calling receive()
    // is designed to stop by itself isStopped() returns false
  }

  /** Create a socket connection and receive data until receiver is stopped */
  private def receive() {
    println("Receiver receive logs")
    try {
      while (!isStopped && lines.hasNext) {
        //     store(userInput)
        var dataItem = lines.next()
        store(dataItem)
        //      userInput = reader.readLine()
      }
      file.close()
      // Restart in an attempt to connect again when server is active again
      restart("Trying to connect again")
    } catch {
      case e: java.net.ConnectException =>
        // restart if could not connect to server
        restart("Error Start Customer Receiver")
      case t: Throwable =>
        // restart if there is any other error
        restart("Error receiving data", t)
    }
  }
}