package olasystem.structure

import java.util
import java.util.Collections
import java.util.concurrent.CopyOnWriteArrayList

import scala.collection.mutable

/**
 * Created by yanjga on 7/13/2015.
 */

class MapSkipList(st: statisticOfTheKey) {
  var statOfTheKey = st
  var rangMin = st.rMin
  var rangeMax = st.rMax
  var bigOffset = (st.rMax - st.rMin) / st.indexCount
  var smallOffset = (st.rMax - st.rMin) / st.listItemCount
  val linkedValueIndex = new Array[util.LinkedList[MapSkipListItem]](st.indexCount)
  var linkedItemCapacity = 0

  def insertItem(item: Double): Unit = {
    var index = (item / bigOffset).toInt
    if(index == linkedValueIndex.size) {
      // the max value case
      index -= 1
    }
    if (linkedValueIndex(index) == null) {
      val list =  new util.LinkedList[MapSkipListItem]()
      var itemValue = new Sum
      itemValue.add(item)
      list.add(new MapSkipListItem(1, itemValue, getSmallOffsetStartIndex(item), getSmallOffsetEndIndex(item)))
      linkedValueIndex(index) = list

    } else {
      var first = linkedValueIndex(index)
      var firstIter = first.iterator()
      var itemValue = new Sum
      itemValue.add(item)
      var flag = true
      var tmpValue: MapSkipListItem = null
      var startIndex = 0
      while (firstIter.hasNext && flag == true) {
        // avoid concurrent exception
        tmpValue = firstIter.next()
        if (item < tmpValue.startRange) {
          first.addFirst(new MapSkipListItem(1, itemValue, getSmallOffsetStartIndex(item), getSmallOffsetEndIndex(item)))
          flag = false
          linkedItemCapacity += 1
        } else if (item > tmpValue.endRange) {
          first.addLast(new MapSkipListItem(1, itemValue, getSmallOffsetStartIndex(item), getSmallOffsetEndIndex(item)))
          flag = false
          linkedItemCapacity += 1
        } else if(item > tmpValue.startRange && item < tmpValue.endRange){
          tmpValue.itemCount += 1
          tmpValue.aggValue.add(item)
          flag = false
        }
      }
    }

    def getSmallOffsetStartIndex(item: Double): Double = {
      (item / smallOffset)
    }

    def getSmallOffsetEndIndex(item: Double): Double = {
      (item / smallOffset) + 1
    }
  }
}

class MapSkipListItem(itemCountC: Int, aggValueC: AggValue, startRangeC: Double, endRangeC: Double) {
  var itemCount: Int = itemCount
  var aggValue: AggValue = aggValueC
  var startRange: Double = startRangeC
  var endRange: Double = endRangeC
}

class statisticOfTheKey {
  var indexCount: Int = 0
  var listItemCount: Int  = 0
  var rMin: Double = 0
  var rMax: Double = 0
}