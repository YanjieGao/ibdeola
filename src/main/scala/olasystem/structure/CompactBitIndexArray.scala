package olasystem.structure

import org.apache.spark.util.collection.BitSet
import java.util
/**
 * Created by yanjga on 7/13/2015.
 */
class CompactBitIndexArray(indexCount: Int, iMin: Double, iMax: Double) extends Serializable{
  var count = indexCount
  val itemMin = iMin
  val itemMax = iMax
  val offset = (itemMax - itemMin) / indexCount
  val bitIndex = new BitSet(indexCount)
  val itemArray = new Array[CompactArrayItem](indexCount)

  def transferMapSkipList2CompactArray(skipList: MapSkipList): Unit = {
    // (1) construct array and bitset
    val iter = skipList.linkedValueIndex.iterator
    var tmpValue: util.LinkedList[MapSkipListItem] = null
    var index = 0
    var startIndex = 0
    var offset = 0
    while(iter.hasNext) {
      tmpValue = iter.next()
      if(tmpValue != null) {
        var tmpIter = tmpValue.iterator()
        while (tmpIter.hasNext) {
          var tmpSkipItem = tmpIter.next()
          itemArray(index) = new CompactArrayItem(tmpSkipItem.itemCount, tmpSkipItem.aggValue)
          bitIndex.set(index)
          index += 1
        }
      }
      index = 0
    }
  }
}

object CompactBitIndexArray {
  def mergeBitSet(left: BitSet, right: BitSet): BitSet = {
    left.`|`(right)
  }

  def combine2CompactBitIndexArray(left: CompactBitIndexArray, right: CompactBitIndexArray): CompactBitIndexArray = {
    var index = left.bitIndex.capacity - 1
    var itemArrayIndex = left.itemArray.size - 1
    while (index >= 0) {
      var cap = left.bitIndex.capacity
      var bitValue = left.bitIndex.get(index)
      if (bitValue) {
        if(left.itemArray(index) == null) {
          println("test")
        }
        left.itemArray(index).itemCount += right.itemArray(itemArrayIndex).itemCount
        left.itemArray(index).aggValue.combine(right.itemArray(itemArrayIndex).aggValue)
        //itemArrayIndex -= 1
      }
      left.bitIndex.`|`(right.bitIndex)
      index -= 1
    }
    left
  }

  def mergeCompactBitIndexArray(arrays: Seq[CompactBitIndexArray]): CompactBitIndexArray = {
    val first = arrays(0)
    var bit = new BitSet(first.bitIndex.capacity)
    arrays.foreach(array => {
      mergeBitSet(bit, array.bitIndex)
    })
    val combinedArray = new CompactBitIndexArray(first.count, first.itemMin, first.itemMax)
    val arrayIter = arrays.iterator
    while (arrayIter.hasNext) {
      var array = arrayIter.next()
      var index = array.bitIndex.capacity
      var itemArrayIndex = array.itemArray.size - 1
      while (index >= 0) {
        var bitValue = array.bitIndex.get(index)
        if (bitValue) {
          combinedArray.itemArray(itemArrayIndex).itemCount += array.itemArray(itemArrayIndex).itemCount
          combinedArray.itemArray(itemArrayIndex).aggValue.combine(array.itemArray(itemArrayIndex).aggValue)
          itemArrayIndex -= 1
        }
        index -= 1
      }
    }
    combinedArray
  }
}

class CompactArrayItem(itemCountC: Int, aggValueC: AggValue) extends Serializable{
  var itemCount: Int = itemCountC
  var aggValue: AggValue = aggValueC
}