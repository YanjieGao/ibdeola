package olasystem.structure

/**
 * Created by yanjga on 7/13/2015.
 */
abstract class AggValue extends Serializable{
  def combine(other: AggValue)
  def add(item: Double)
  def getResult: Double
}

class Avg extends AggValue {
  var sum = 0.0
  var count = 0
  override def combine(other: AggValue) = {
    val otherAvg = other.asInstanceOf[Avg]
    sum += otherAvg.sum
    count += otherAvg.count
  }

  override def add(item: Double): Unit ={
    sum += item
    count += 1
  }

  override def getResult: Double = {
    sum / count
  }
}

class Sum extends AggValue {
  var sum = 0.0
  override def combine(other: AggValue) = {
    sum += other.asInstanceOf[Sum].sum
  }

  override def add(item: Double) = {
    sum += item
  }

  override def getResult(): Double = {
    sum
  }
}

class Count extends AggValue {
  var count = 0
  override def combine(other: AggValue) = {
    count += other.asInstanceOf[Count].count
  }

  override def add(item: Double) = {
    count += 1
  }

  override def getResult: Double = {
    count
  }
}