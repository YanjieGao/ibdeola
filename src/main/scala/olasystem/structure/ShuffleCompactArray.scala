package olasystem.structure

import olasystem.util.{TreeUtils, OLABitSet}
import org.apache.spark.SparkException
import org.apache.spark.util.collection.BitSet

import scala.collection.mutable.ArrayBuffer

/**
 * Created by yanjga on 7/16/2015.
 */
class ShuffleCompactArray(arraySize: Int, needSum: Boolean, minC: Double, maxC: Double, rangeIndexC: Array[Short]) extends Serializable {
  var rangeIndex: Array[Short] = null // store: start range and end range
  //var rangeIndex: Array[Short] = null // store: start range and end range
  var compactDataSum: Array[Double] = null // store: sum
  var compactDataCount: Array[Int] = null
  var needSumFlag: Boolean = false

  // Here Need to Config or preBuild this
  var min: Double = 0.0
  var max: Double = 10000.0
  var leafCount: Int = 0 //  the logic all the leafs
  var treeNodesCount: Int = 0
  var leafStartIndex: Int = 0

  var offset = 0.0

  var compressTreeSchema: String = null
  var bitSetTreeSchema: OLABitSet = null
  //var bitIndex: OLABitSet = null
  // start Construct Data
  rangeIndex = rangeIndexC

  leafCount = arraySize / 2
  treeNodesCount = leafCount * 2 - 1
  leafStartIndex = (treeNodesCount + 1) - leafCount
  //bitIndex = olaBitIndex
  if(needSum) {
    compactDataSum = new Array[Double](treeNodesCount + 1) // store: sum
    needSumFlag = needSum
  }
  compactDataCount = new Array[Int](treeNodesCount + 1)

  min = minC
  max = maxC
  offset = (max - min) / leafCount

  // end Construct Data
  def accumulateItem(item: Double): Unit = {
    var index = ((item - min)/ offset).toInt
    if(index > compactDataCount.size - 1) {
      index = compactDataCount.size - 1
    }
    compactDataCount(index + leafCount) += 1
    if(needSum) {
      compactDataSum(index + leafCount) += item
    }
  }

  def fromTop2BottomUnsetBitSet(bitSet: OLABitSet, leafStartIndex: Int, leafEndIndex: Int): OLABitSet = {
    val iter = bitSet.iterator
    var current = 0
    while(iter.hasNext) {
      var index = iter.next()
      current = index * 2
      while (current < leafEndIndex) {
        val left = current
        val right = current + 1
        bitSet.unset(left)
        bitSet.unset(right)
      }
    }
    bitSet
  }

  def findMostGeneralBitIndex(arrays: Seq[ShuffleCompactArray]): OLABitSet = {
    var mostGeneralArray: ShuffleCompactArray = null
    // | all the bitSet
    val bitSets = arrays.map(
      item =>
        // if (item.bitSetTreeSchema == null) {
        //        new OLABitSet(item.leafCount)
        //      } else {
        item.bitSetTreeSchema
      //}
    )
    var combineResult: OLABitSet = null
    if (arrays.size == 1) {
      combineResult = bitSets(0)
    } else {
      combineResult = bitSets.fold(bitSets(0))((a, b) => a.|(b))
      // from bottom to top merge the bit flag
      // tree root node flag logical start with 1
      // tree root node flag phythical start with 0
      combineResult = fromTop2BottomUnsetBitSet(combineResult, arrays(0).leafStartIndex, arrays(0).treeNodesCount)
      var tail = arrays(0).treeNodesCount
        //combineResult.capacity
      var parent = 0
      // clear the redundant node
      while (tail > 0) {
        if (tail % 2 == 0) {
          parent = tail / 2
          val parentFlag = combineResult.get(parent)
          val childsFlag = (combineResult.get(tail + 1) && combineResult.get(tail))
          if (parentFlag) {
            combineResult.unset(tail + 1)
            combineResult.unset(tail)
          }
        }
        tail -= 1
      }
    }
    combineResult
  }

  // the method to combine multi structure tree.
  def combineMultiRangeBitSetAndArray(mostGeneralTree: OLABitSet, currentShuffleArraySet: Seq[ShuffleCompactArray], historyShuffleArray: ShuffleCompactArray): (OLABitSet, ShuffleCompactArray) = {
    val mainBitIter = mostGeneralTree.iterator
    var currentRootNodeIndex = -1
    var end = -1
    if(mainBitIter.hasNext) {
      currentRootNodeIndex= mainBitIter.next()
    }
    val leafStart = currentShuffleArraySet(0).leafStartIndex
    val leafEnd = currentShuffleArraySet(0).treeNodesCount
//    while (mainBitIter.hasNext) {
//      // combine item from start to end
//     //if(currentRootNodeIndex != -1) {
//       // end = mainBitIter.next()
      //  var (start, end): (Int, Int) = TreeUtils.fromRootIndex2LeafStartAndEnd(currentRootNodeIndex, leafStart, leafEnd)
        combineMultiArrayThroughRange(historyShuffleArray, currentShuffleArraySet)
       // currentRootNodeIndex = mainBitIter.next()
//      } else {
//        //start = mainBitIter.next()
//        currentRootNodeIndex = mainBitIter.next()
//      }
    //}
//    if(currentRootNodeIndex != -1) {
//      // combine item from start to the tail
//      var (start, end): (Int, Int) = TreeUtils.fromRootIndex2LeafStartAndEnd(currentRootNodeIndex, leafStart, leafEnd)
//      combineMultiArrayThroughRange(historyShuffleArray, currentShuffleArraySet, start, end)
//    }
   // historyShuffleArray.CompressDataUseBitSet()
    (mostGeneralTree, historyShuffleArray)
  }

  def fromLeaf2NearestParent(leafIndex: Int, bitIndex: OLABitSet) : Int = {
    var index = 0
    if(index % 2 == 0) {
      index = leafIndex
    } else {
      index = leafIndex - 1
    }
    var flag = true
    while (index > 0 && flag) {
      if(bitIndex.get(index)) {
        flag = false
      } else {
        index / 2
      }
    }
    if(index == 0) {
      if(bitIndex.get(0)) {
        index
      } else {
        leafIndex
      }
    } else {
      index
    }
  }

  def transferCurrentIndex2CompressArrayIndex(currentIndex: Int, bitSet: OLABitSet): Int = {
    var index = 0
    val iter = bitSet.iterator
    var flag = true
    while (iter.hasNext && flag) {
      val tmpIndex = iter.next()
      if(currentIndex == tmpIndex ) {
        flag = false
      } else {
        index += 1
      }
    }
    index
  }

  def combineMultiArrayThroughRange(baseArray: ShuffleCompactArray, newArrays: Seq[ShuffleCompactArray]): ShuffleCompactArray = {
    val arrayIter = newArrays.iterator
    //var tmpIndex = start
    try {
      while (arrayIter.hasNext) {
        val currentArrayData = arrayIter.next()
        val currentArrayBitIter = currentArrayData.bitSetTreeSchema.iterator
        while (currentArrayBitIter.hasNext) {
          var currentIndex = currentArrayBitIter.next()
          var baseParent = fromLeaf2NearestParent(currentIndex, baseArray.bitSetTreeSchema)
          //val compressParentIndex = transferCurrentIndex2CompressArrayIndex(baseParent, baseArray.bitSetTreeSchema)
          var compressIndex = transferCurrentIndex2CompressArrayIndex(currentIndex, currentArrayData.bitSetTreeSchema)
          if(currentArrayData.compactDataCount.size <= compressIndex) {
            println("")
          }
          val countResult = currentArrayData.compactDataCount(compressIndex)
          baseArray.compactDataCount(baseParent) = countResult
          if (needSumFlag) {
            baseArray.compactDataSum(baseParent) = currentArrayData.compactDataSum(compressIndex)
          }
          //        if(currentIndex >= start && currentIndex <= end) {
          //          baseArray.compactDataCount(start) = currentArrayData.compactDataCount(currentIndex)
          //          if(needSumFlag) {
          //            baseArray.compactDataSum(start) = currentArrayData.compactDataSum(currentIndex)
          //          }
          //        }
        }
      }
      baseArray
   }
    catch {
      case e: Exception =>
        throw e
    }
    finally {

    }
  }

  def combine(other: ShuffleCompactArray): ShuffleCompactArray = {
    // Think the situation, as the tree is dynamic
    // (1) both have equal leaf nodes size
    if(leafCount == other.leafCount) {
      var tmpIndex = leafCount - 1
      while(tmpIndex > 0) {
        compactDataCount(tmpIndex) += other.compactDataCount(tmpIndex)
        if(needSumFlag) {
          compactDataSum(tmpIndex) += other.compactDataCount(tmpIndex)
        }
        tmpIndex -= 1
      }
    }
    // (2) both have different leaf nodes size
    this
  }
  // Init the ShuffleCompactArray
  def ShuffleCompactArray(): Unit = {
    //rangeIndex = rangeIndexC
    //bitIndex = olaBitIndex
    if(needSum) {
      compactDataSum = new Array[Double](arraySize) // store: sum
      needSumFlag = needSum
    }
    compactDataCount = new Array[Int](arraySize)

    leafCount = arraySize
    min = minC
    max = maxC
    offset = (max - min) / leafCount
  }

  def FromBottom2TopConstructCountTreeArray(data: Array[Int]): Array[Int] = {
    //val iter =
    data
  }

  //  def CompressDataStore(): Unit = {
  ////    var  nonZeroCount = 0
  ////    compactDataCount.foreach(item => {
  ////      if(nonZeroCount != 0) nonZeroCount += 1;
  ////    })
  //    // Here don't compress the zero tree nodes.
  //    val treeNodeSize = rangeIndex.size
  //    // allocate tmp memory store
  //    //var rangeIndexT = new Array[Short](nonZeroCount * 2) // store: start range and end range
  //    var compactDataSumT: Array[Double] = null
  //    if(needSumFlag) {
  //      compactDataSumT = new Array[Double](treeNodeSize) // store: sum
  //      needSumFlag = needSumFlag
  //    }
  //    var compactDataCountT = new Array[Int](treeNodeSize)
  //    // compress data to the new array
  //    var i = 0
  //    var start = 0
  //    var end = 0
  //    while(end < treeNodeSize) {
  //      start = rangeIndex(i)
  //      end = rangeIndex(i + 1) - 1
  //      while(start <= end) {
  //        compactDataCountT(i) += compactDataCount(start)
  //      }
  //      i += 1
  //    }
  //
  //    if (needSumFlag) {
  //      var i = 0
  //      var start = 0
  //      var end = 0
  //      while(end < treeNodeSize) {
  //        start = rangeIndex(i)
  //        end = rangeIndex(i + 1) - 1
  //        while(start <= end) {
  //          compactDataSumT(i) += compactDataSum(start)
  //        }
  //        i += 1
  //      }
  //    }
  //  }
  def CompressData(): Unit = {
    // CompressCount
//    var tmpCompactDataCount = new Array[Int](bitIndex.cardinality())
//    var iter = bitIndex.iterator
//    while (iter.hasNext) {
//
//    }
//    // CompressSum
//    if(needSum) {
//      var tmpCompactDataSum = new Array[Double](bitIndex.cardinality())
//    }
  }

  def CompressDataUseBitSet(): Unit = {
    if(bitSetTreeSchema == null) {
      bitSetTreeSchema = new OLABitSet(arraySize)
      bitSetTreeSchema = transformString2BitSet(compressTreeSchema, bitSetTreeSchema)
    }
    // merge 1 -> before next 1
    val bitIter = bitSetTreeSchema.iterator
    //var start = -1
    //var end = -1
    var current = 0
    var nextBatchFlag = false

    var tmpCompactDataCount = new ArrayBuffer[Int]()
    var tmpCompactDataSum: ArrayBuffer[Double] = null
    if(needSum) {
      tmpCompactDataSum = new ArrayBuffer[Double]()
    }
    //    while(bitIter.hasNext && current < 1) {
    //      var bitFlag = bitIter.next()
    //      start = bitFlag
    //      current += 1
    //    }
    while (bitIter.hasNext) {
      var bitFlag = bitIter.next()
      var (startIndex, endIndex) = fromRootIndex2LeafStartAndEnd(bitFlag, leafStartIndex, treeNodesCount)
      //if (end <= start) {
      //end = bitFlag
      var countFlag = mergeCount(startIndex, endIndex)
     // if (countFlag != 0) {
        tmpCompactDataCount += countFlag
//      } else {
//        //bitSetTreeSchema.set()
//      }
      if (needSumFlag) {
        var sumFlag = mergeSum(startIndex, endIndex)
    //    if (sumFlag != 0) {
          tmpCompactDataSum += sumFlag
    //    }
      }
      //} else {
      //   startIndex = bitFlag
      //}
    }

    compactDataCount = tmpCompactDataCount.toArray
    if (tmpCompactDataSum != null) {
      compactDataSum = tmpCompactDataSum.toArray
    }
  }

  def fromRootIndex2LeafStartAndEnd(rootIndex: Int, leafStart: Int, leafEnd: Int): (Int, Int) = {
    var start = rootIndex
    var end = rootIndex
    while(!(start >= leafStart && start <= leafEnd)) {
      start *= 2
    }
    while(!(end >= leafStart && end <= leafEnd)) {
      end *= 2
      end += 1
    }
    (start, end)
  }

  def transformBitSet2String(treeBitSet: OLABitSet): String = {
    val iter = treeBitSet.iterator
    var result = new StringBuffer()
    var start = 0
    var flag = 0
    while (iter.hasNext) {
      flag = iter.next()
      while(start < flag) {
        result.append('0')
        start += 1
      }
      result.append('1')
      start = flag + 1
    }
    if(flag < treeBitSet.bitSize) {
      while(start < treeBitSet.bitSize) {
        result.append('0')
        start += 1
      }
    }
    result.toString
  }

  def transformString2BitSet(stringTree: String, treeBitSet: OLABitSet): OLABitSet = {
    val iter = stringTree.iterator
    var index = 0
    while(iter.hasNext) {
      val flag = iter.next()
      if(flag == '1') {
        treeBitSet.set(index)
      }
      index += 1
    }
    treeBitSet
  }

  def mergeCount(start: Int, end: Int): Int = {
    try {
      var tmpResult: Int = 0
      for (i <- start to end) {
        if(compactDataCount.size <= i) {
          println("")
        }
        tmpResult += compactDataCount(i)
      }
      if (start == end) {
        tmpResult = compactDataCount(start)
      }
      tmpResult
    }
    catch {
      case e: Exception =>
        throw e
    }
    finally {

    }
  }

  def mergeSum(start: Int, end: Int) : Double = {
    var tmpResult: Double = 0
    for(i <- start to end) {
      tmpResult += compactDataSum(i)
    }
    tmpResult
  }
}
