package olasystem.structure

import java.util

import olasystem.util.OLABitSet
import org.apache.spark.util.collection.BitSet

/**
 * Created by yanjga on 7/14/2015.
 */
class ErrorBoundTree(treeDepth: Int, minC: Double, maxC: Double, errorBound: Double) extends Serializable{
  val keyGlobalErrorBound = errorBound
  val min = minC
  val max = maxC
  val treeDepthE = treeDepth
  var nodeNumbers = (math.pow(2, treeDepthE) - 1).toInt
    //treeDepth2NodesNumber(treeDepth)
  // Tree from the 1 as the beginning index
  var treeArray: Array[ErrorBoundTreeItem] = new Array[ErrorBoundTreeItem](nodeNumbers + 1)
  var bitMarkedIndex: OLABitSet = new OLABitSet(nodeNumbers + 1)

  // Use BFS to travel init the tree, Index from 1 begin
  def initTreeArray(min: Double, max: Double): Unit = {
    // (1) init the Tree
    treeArray = treeArray.map(item => {
      new ErrorBoundTreeItem(0.0, 0.0, 0, 0.0)
    })
    var queue = new util.LinkedList[Int]
    queue.add(1)
    var currentQueue = new util.LinkedList[Int]()
    var tmpQueue: util.LinkedList[Int] = null
    // (2) update the tree node min and max value
    if(treeArray.size <= 1) {
      print()
    }
    treeArray(1).min = min
    treeArray(1).max = max
    treeArray(1).count = 0
    while(!queue.isEmpty && queue != null) {
      while(!queue.isEmpty && queue != null) {
        var root = queue.poll()
        var leftIndex = root * 2
        var rightIndex = root * 2 + 1
        if(leftIndex <= treeArray.size && rightIndex <= treeArray.size) {
          var offsetMaxMin = treeArray(root).max - treeArray(root).min
          treeArray(leftIndex).min = treeArray(root).min
          treeArray(leftIndex).max = offsetMaxMin / 2 + treeArray(root).min
          treeArray(rightIndex).min = offsetMaxMin / 2 + treeArray(root).min
          treeArray(rightIndex).max = treeArray(root).max
          currentQueue.add(leftIndex)
          currentQueue.add(rightIndex)
        }
      }
      tmpQueue = queue
      queue = currentQueue
      currentQueue = tmpQueue
    }
  }
  // Shuffle Compact Array Update Method
  def top2DownUpdateTreeShuffleCompact(compactArray: ShuffleCompactArray): Unit = {
    //var compactIter = compactArray.compactDataCount.iterator
    var index = 0
    var offset = compactArray.offset
    while(index <= compactArray.treeNodesCount) {
      //var leafNodeItem = compactIter.next()
      if(compactArray.compactDataCount(index) != 0) {
        treeArray(index).count += compactArray.compactDataCount(index)
      }
      //updateLeafNodeItem2ErrorBoundTreeShuffleCompact(leafNodeItem, index, offset)
      index += 1
    }
  }

//  def updateLeafNodeItem2ErrorBoundTreeShuffleCompact(leafNodeItemCount: Int, leafIndex: Int, offset: Double): Unit = {
//    var errorTreeIter = treeArray.iterator
//    var index = 1
//    treeArray(1).count += leafNodeItemCount
//    var leafStart = leafIndex * offset
//    var leafEnd = leafIndex * offset
//    while((index * 2 + 1) < nodeNumbers) {
//      // (1) the node should udpate the left sub tree
//      if(!bitMarkedIndex.get(index)) {
//        if (treeArray(index * 2).min <= (leafStart) && treeArray(index * 2).max >= leafEnd) {
//          treeArray(index * 2).count += leafNodeItemCount
//        } else {
//          // (2) the node should update the right sub tree
//          treeArray(index * 2 + 1).count += leafNodeItemCount
//        }
//      } else {
//        // stop travel
//        index = nodeNumbers
//      }
//      index *= 2
//    }
//  }

  // Compact Array Update Method
  def top2DownUpdateTree(compactArray: CompactBitIndexArray): Unit = {
    var compactIter = compactArray.itemArray.iterator
    var index = 0
    var offset = compactArray.offset
    while(compactIter.hasNext) {
      var leafNodeItem = compactIter.next()
      updateLeafNodeItem2ErrorBoundTree(leafNodeItem, index, offset)
      index += 1
    }
  }

  def down2TopEstimateErrorBound(): Unit = {
    var left = nodeNumbers - 1
    var right = nodeNumbers
    var root = left / 2
    while(root > 0){
      // There are at least one node not reach errorbound
      var rootReachErrorBoundFlag = treeArray(root).reachErrorBoundFlag
      if(!rootReachErrorBoundFlag) {
        judgeTriangleErrorBound(root, left, right)
      }
      right = left - 1
      left = right - 1
      root = left / 2
    }
  }

  def checkErrorBoundRoot(): Boolean ={
    // tree index start from 1
    bitMarkedIndex.get(1)
  }

  // Use Hoeffding model to compute error bound
  def comErrorBound(lower: Double, upper: Double, num: Int): Double = {
    return math.abs((upper - lower) * math.sqrt(math.log(2 / (1 - 0.95)) / (2 * num)))
  }

  def judgeTriangleErrorBound(root: Int, left: Int, right: Int): Unit = {
    // define some rule to determine the three nodes which to stop
    val leftNode = treeArray(left)
    val rightNode = treeArray(right)
    val rootNode = treeArray(root)
    if(rootNode.count < (leftNode.count + rightNode.count)) {
      rootNode.count = leftNode.count + rightNode.count
    }
    leftNode.errorBound = comErrorBound(leftNode.min, leftNode.max, leftNode.count)
    rightNode.errorBound = comErrorBound(rightNode.min, rightNode.max, rightNode.count)
    rootNode.errorBound = comErrorBound(rootNode.min, rootNode.max, rootNode.count)
    // (1) left
    if(leftNode.errorBound <= keyGlobalErrorBound) {
      bitMarkedIndex.unset(left)
      leftNode.reachErrorBoundFlag = true
    }
    // (2) right
    if(rightNode.errorBound <= keyGlobalErrorBound) {
      bitMarkedIndex.unset(right)
      rightNode.reachErrorBoundFlag = true
    }
    // (3) root
    if(rootNode.errorBound <= keyGlobalErrorBound) {
      // (3.1) root node reach errorbound
      bitMarkedIndex.unset(root)
      if(root % 2 == 0) {
        bitMarkedIndex.set(root / 2)
      } else {
        bitMarkedIndex.set((root -1) / 2)
      }
      rootNode.reachErrorBoundFlag = true
    } else if(!bitMarkedIndex.get(left) && !bitMarkedIndex.get(right)) {
      // (3.2) root not reach, but left and right all reach
      bitMarkedIndex.unset(root)
      if(root % 2 == 0) {
        bitMarkedIndex.set(root / 2)
      } else {
        bitMarkedIndex.set((root -1) / 2)
      }
      rootNode.reachErrorBoundFlag = true
    }
  }

  def updateLeafNodeItem2ErrorBoundTree(leafNodeItem: CompactArrayItem, leafIndex: Int, offset: Double): Unit = {
    var errorTreeIter = treeArray.iterator
    var index = 1
    treeArray(1).count += leafNodeItem.itemCount
    var leafStart = leafIndex * offset
    var leafEnd = leafIndex * offset
    while((index * 2 + 1) < nodeNumbers) {
      // (1) the node should udpate the left sub tree
      if(!bitMarkedIndex.get(index)) {
        if (treeArray(index * 2).min <= (leafStart) && treeArray(index * 2).max >= leafEnd) {
          treeArray(index * 2).count += leafNodeItem.itemCount
        } else {
          // (2) the node should update the right sub tree
          treeArray(index * 2 + 1).count += leafNodeItem.itemCount
        }
      } else {
        // stop travel
        index = nodeNumbers
      }
      index *= 2
    }
  }

  def treeDepth2NodesNumber(treeDepth: Int): Int = {
    val result = 2 ^ treeDepth - 1
    result
  }

  def nodesNumber2TreeDepth(nodesNumber: Int): Int = {
    math.log(nodesNumber).toInt
  }
}

class ErrorBoundTreeItem(maxC: Double, minC: Double, countC: Int, errorBoundC: Double) extends Serializable{
  var max = maxC
  var min = minC
  var count = countC
  var errorBound = errorBoundC
  var reachErrorBoundFlag = false
}