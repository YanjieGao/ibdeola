package olasystem.structure

import scala.collection.immutable.HashMap

/**
 * Created by yanjga on 8/20/2015.
 */
class MergeTree {
  var root: MergeTreeNode = null
  var leafMap: HashMap[Int, MergeTreeNode] = null // StartRange - Object Ref

  var startRange: Double = 0.0
  var endRange: Double = 0.0

  var treeDepth: Int = 0
  var leafCount: Int = 0
}

class MergeTreeNode {
  // Tree Structure
  var parent: MergeTreeNode = null
  var leftChild: MergeTreeNode = null
  var rightChild: MergeTreeNode = null
  // Statistic Data
  var count: Int = 0
  var sum: Double = 0.0
  var startRangeIndex: Int = 0
  var endRangeIndex: Int = 0
}