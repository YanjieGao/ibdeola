filepath=$(cd "$(dirname "$0")"; pwd)

cd $filepath && cd .. && cd .. && cd util && chmod 777 config-env.sh && ./config-env.sh
PSSH_HOME=/home/dm/yanjie/exp/pssh
IBDE_HOME=$IBDE_PROJECT_PATH
IBDE_SPARK_HOME=/home/dm/yanjie/exp/spark-1.2.0-bin-hadoop2.4
JAVA_HOME=/home/dm/yanjie/exp/jdk1.7.0_65
SPARK_MASTER=spark://10.190.172.43:7077
echo "start ola.ola.adapterTopKError shell1"

echo "[exp]start exp3-param1"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp3/conf/param1/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp3/log/log1
echo "[exp]stop exp3-param1"

echo "[exp]start exp3-param2"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp3/conf/param2/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp3/log/log2
echo "[exp]stop exp3-param2"

echo "[exp]start exp3-param3"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp3/conf/param3/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp3/log/log3

echo "[exp]stop exp3-param3"
echo "stop ola.ola.adapterTopKError shell1"

echo "[exp]start exp3-baseline1"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp3/conf/baseline1/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp3/log/log1Base
echo "[exp]stop exp3-baseline1"

echo "[exp]start exp3-baseline2"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp3/conf/baseline2/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp3/log/log2Base
echo "[exp]stop exp3-baseline2"

echo "[exp]start exp3-baseline3"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp3/conf/baseline3/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp3/log/log3Base

echo "[exp]stop exp3-baseline3"
echo "stop ola.ola.adapterTopKError shell1"