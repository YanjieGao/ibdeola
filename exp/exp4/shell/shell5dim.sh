filepath=$(cd "$(dirname "$0")"; pwd)
cd $filepath && cd .. && cd .. && cd util && chmod 777 config-env.sh && ./config-env.sh
PSSH_HOME=/home/dm/yanjie/exp/pssh
IBDE_HOME=$IBDE_PROJECT_PATH
IBDE_SPARK_HOME=/home/dm/yanjie/exp/spark-1.2.0-bin-hadoop2.4
JAVA_HOME=/home/dm/yanjie/exp/jdk1.7.0_65
SPARK_MASTER=spark://10.190.172.43:7077
echo "start ola.ola.adapterTopKError shell1"
echo "[exp]start exp4-param1"

#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param0/ola-config.properties $IBDE_HOME/conf
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param0/partitionIndexColumn.properties $IBDE_HOME/conf

#run suite

cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp4/log/log0

#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param20/ola-config.properties $IBDE_HOME/conf
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param20/partitionIndexColumn.properties $IBDE_HOME/conf

#run suite

cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp4/log/log20

#
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param30/ola-config.properties $IBDE_HOME/conf
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param30/partitionIndexColumn.properties $IBDE_HOME/conf

#run suite

cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp4/log/log30

#

#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param0base/ola-config.properties $IBDE_HOME/conf
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param0base/partitionIndexColumn.properties $IBDE_HOME/conf

#run suite

cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp4/log/log0base

#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param20base/ola-config.properties $IBDE_HOME/conf
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param20base/partitionIndexColumn.properties $IBDE_HOME/conf

#run suite

cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp4/log/log20base

#
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param30base/ola-config.properties $IBDE_HOME/conf
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp4/conf/param30base/partitionIndexColumn.properties $IBDE_HOME/conf

#run suite

cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp4/log/log30base

#