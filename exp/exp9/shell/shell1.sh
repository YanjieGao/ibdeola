filepath=$(cd "$(dirname "$0")"; pwd)

cd $filepath && cd .. && cd .. && cd util && chmod 777 config-env.sh && ./config-env.sh
PSSH_HOME=/home/dm/yanjie/exp/pssh
IBDE_HOME=$IBDE_PROJECT_PATH
IBDE_SPARK_HOME=/home/dm/yanjie/exp/spark-1.2.0-bin-hadoop2.4
JAVA_HOME=/home/dm/yanjie/exp/jdk1.7.0_65
SPARK_MASTER=spark://10.190.172.43:7077
echo "start ola.ola.adapterTopKError shell1"
echo "[exp]start exp9-baseline1"
#config



echo "[exp]start exp9-param1"
#config
cd $PSSH_HOME && ./pssh - h hosts.txt -P "rm $IBDE_HOME/conf/ola-config.properties"
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/param1/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log1
echo "[exp]stop exp9-param1"

echo "[exp]start exp9-param2"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/param2/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log2
echo "[exp]stop exp9-param2"

echo "[exp]start exp9-param3"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/param3/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log3

echo "[exp]stop exp9-param3"

echo "[exp]start exp9-param4"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/param4/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log4
echo "[exp]stop exp9-param4"

echo "[exp]start exp9-param5"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/param5/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log5
echo "[exp]stop exp9-param5"

echo "stop ola.ola.baseline shell1"

cd $PSSH_HOME && ./pssh -h hosts.txt -P "rm $IBDE_HOME/conf/ola-config.properties"
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/baseline1/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log1base
echo "[exp]stop exp9-baseline1"

echo "[exp]start exp9-baseline2"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/baseline2/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log2base
echo "[exp]stop exp9-baseline2"

echo "[exp]start exp9-baseline3"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/baseline3/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log3base

echo "[exp]stop exp9-baseline3"

echo "[exp]start exp9-baseline4"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/baseline4/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log4base
echo "[exp]stop exp9-baseline4"

echo "[exp]start exp9-baseline5"

#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp9/conf/baseline5/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_Experiment   --master $SPARK_MASTER   --executor-memory 40G --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp9/log/log5base
echo "[exp]stop exp9-baseline5"