filepath=$(cd "$(dirname "$0")"; pwd)

cd $filepath && cd .. && cd .. && cd util && chmod 777 config-env.sh && ./config-env.sh
PSSH_HOME=/home/dm/yanjie/exp/pssh
IBDE_HOME=$IBDE_PROJECT_PATH
IBDE_SPARK_HOME=/home/dm/yanjie/exp/spark-1.2.0-bin-hadoop2.4
JAVA_HOME=/home/dm/yanjie/exp/jdk1.7.0_65
SPARK_MASTER=spark://10.190.172.43:7077
#./bin/spark-submit   --class workload.IBDE_TestIndexIterative   --master $SPARK_MASTER   --executor-memory 40G --driver-memory 40g  --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > testiterative

echo "start ola.ola.IndexIterative shell1"

echo "[exp]start exp10-baseline1"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp10/conf/baseline1/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_TestIndexIterative   --master $SPARK_MASTER   --executor-memory 40G --driver-memory 40g  --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp10/log/log1Base
echo "[exp]stop exp10-baseline1"

echo "[exp]start exp10-baseline2"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp10/conf/baseline2/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_TestIndexIterative   --master $SPARK_MASTER   --executor-memory 40G --driver-memory 40g  --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp10/log/log2Base
echo "[exp]stop exp10-baseline2"

echo "[exp]start exp10-baseline3"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp10/conf/baseline3/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_TestIndexIterative   --master $SPARK_MASTER   --executor-memory 40G --driver-memory 40g --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp10/log/log3Base
echo "[exp]stop exp10-baseline3"

echo "[exp]start exp10-param1"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp10/conf/param1/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_TestIndexIterative   --master $SPARK_MASTER   --executor-memory 40G --driver-memory 40g  --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp10/log/log1
echo "[exp]stop exp10-param1"

echo "[exp]start exp10-param2"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp10/conf/param2/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_TestIndexIterative   --master $SPARK_MASTER   --executor-memory 40G  --driver-memory 40g --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp10/log/log2
echo "[exp]stop exp10-param2"

echo "[exp]start exp10-param3"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/exp10/conf/param3/ola-config.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit   --class workload.IBDE_TestIndexIterative   --master $SPARK_MASTER   --executor-memory 40G --driver-memory 40g --conf spark.akka.frameSize=1024    $IBDE_HOME/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar > $IBDE_HOME/exp/exp10/log/log3

echo "[exp]stop exp10-param3"
echo "stop ola.ola.IndexIterative shell1"