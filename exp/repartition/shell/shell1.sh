filepath=$(cd "$(dirname "$0")"; pwd)
cd $filepath && cd .. && cd .. && cd util && chmod 777 config-env.sh && ./config-env.sh
PSSH_HOME=/home/dm/yanjie/exp/pssh
IBDE_HOME=$IBDE_PROJECT_PATH
IBDE_SPARK_HOME=/home/dm/yanjie/exp/spark-1.2.0-bin-hadoop2.4
JAVA_HOME=/home/dm/yanjie/exp/jdk1.7.0_65
SPARK_MASTER=spark://10.190.172.43:7077
echo "start ola.ola.IndexIterative shell1"

echo "[exp]start repartition"
#config
cd $PSSH_HOME && ./pscp -h hosts.txt $IBDE_HOME/exp/repartition/conf/partitionIndexColumn.properties $IBDE_HOME/conf
#run suite
cd $IBDE_SPARK_HOME && ./bin/spark-submit \
  --class workload.IBDE_Repartition \
  --master $SPARK_MASTER \
  --executor-memory 40G \
  $IBDE_PROJECT_PATH/target/scala-2.10/KafkaSpark-assembly-0.0.1.jar
echo "[exp]stop repartition"